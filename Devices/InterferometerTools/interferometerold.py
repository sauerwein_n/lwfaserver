# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 10:46:54 2016

@author: hermann_b
"""
import numpy as np
import pypylon
import matplotlib.pyplot as plt
from scipy import misc
from scipy import ndimage
import time
import os 
import sys
sys.path.append('C:\\Users\\hermann_b-adm\\OneDrive - ETHZ\\MA')
import routines as r

# camera setup
available_cameras = pypylon.factory.find_devices()
print(available_cameras)
cam = pypylon.factory.create_device(available_cameras[0])
if (cam.opened): print('cam opened')
cam.properties['ExposureTimeAbs']=25
cam.properties['GevSCPSPacketSize']=8000
cam.properties['TriggerMode']='On'
cam.properties['AcquisitionFrameRateEnable']=False
cam.properties['AcquisitionFrameRateAbs']=100
cam.properties['GainRaw']=150

# optical parameters
X_nozzle=1300
psize=1./127.95 #mm
mm=int(1./psize)
ep=1.74/psize

#parameters for live preview
x1=6.0
x2=2.6
y1=3
y2=12
x1p=int(1750-x1/psize)
x2p=int(1750-x2/psize)
y1p=int(y1/psize)
y2p=int(y2/psize)
print(x1p)

def Alpha(D1, D2):
    sigmax=5
    sigmay=0.5
    Ds1=ndimage.filters.gaussian_filter(np.array(D1,dtype=float), [sigmax, sigmay], mode='nearest', truncate=2)
    Ds2=ndimage.filters.gaussian_filter(np.array(D2,dtype=float), [sigmax, sigmay], mode='nearest', truncate=2)
    alpha=-r.unwrap(Ds1[x1p:x2p,:],Ds2[x1p:x2p,:])[:,y1p:y2p]
    return alpha
    
def take_phase_img(path=None, mode='live'):

    D=[]
    i=0
    not_found = True
    while not_found:    
        cam.open()
        try:
            for img in cam.grab_images(2):
                #print(np.max(img), np.min(img))
                if path != None: 
                    misc.imsave(path+str(i)+'.jpg',img)
                D.append(img)
                i+=1
                not_found = False
        except:
            cam.close()
            time.sleep(1)
            print ('No image found')
        
    if mode=='live':  #live phase image
        plt.figure('InterferometerLive', figsize=(12,5))
        plt.clf()
        plt.subplot(121)
        plt.cla()
        plt.imshow(D[0], extent=psize*np.array([0.,img.shape[1], 0., img.shape[0]]))
        alpha= Alpha(D[0],D[1])
        plt.subplot(122)
        plt.cla()
        plt.imshow(alpha, extent=[y1, y2, x2, x1])
        #plt.imshow(D[1], extent=psize*np.array([0.,img.shape[1], 0., img.shape[0]]))
        plt.colorbar(shrink=0.3)
        plt.title("Phase")  
        plt.pause(0.05)
    
while True:
    take_phase_img()

p="C:\\Users\\hermann_b-adm\\OneDrive - ETHZ\\MA\\DataLWFA\\2202\\Interferometer\\"
#take_phase_img(path=p , mode="live")

cam.close()