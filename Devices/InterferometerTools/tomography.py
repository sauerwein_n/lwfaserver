import tomopy

import numpy as np

import matplotlib.pyplot as plt
import time as t

def calculate_phase(images_angles, Ly,center = 230, y_calib=200, slices=None, plot_images = False, plot_phases = False,shift=217):
    
    y1 = center - Ly//2 - y_calib
    y2 = center + Ly//2 + y_calib
    
    from Devices.InterferometerTools.routines import phase, unwrap, correct_phase_jumps
    
    if plot_images:
        plt.figure()
        plt.imshow(images_angles[-1][0][:,int(y1):int(y2)])
        plt.show()
    
    if not slices:
        slices=range(len(images_angles[0][0]))

    phase_angles = []
    
    #print (Ly, y1, y2, images_angles[0][0].shape[1])
    
    center = center - int(y1) #if calibration is available. 0-180
    for idx, images_angle in enumerate(images_angles):
        
        #loop trough the needed heights
        alpha = unwrap(images_angle[0][slices,:], images_angle[1][slices,:])[:,int(y1):int(y2)]
        alpha = correct_phase_jumps(alpha) #correct for phases larger than 2pi

        #from scipy import ndimage
        #alpha = ndimage.filters.gaussian_filter(alpha,[0,15], mode="nearest", truncate=2)
        
        
        phase_l = phase(alpha, alpha, shift=shift)
        
        phase_r = -phase(alpha[:,::-1], alpha[:,::-1], shift=shift)[:,::-1]
        
        phase_l = (phase_l[:,:-shift] + phase_r[:,shift:])/2.  
        
        x = np.arange(phase_l.shape[1])
        
        fx1 = np.average(phase_l[:,:y_calib])
        fx2 = np.average(phase_l[:,-y_calib:])
        
        fs = (x - y_calib//2)/(Ly + y_calib) * fx2 - (x - (Ly + (3 * y_calib)//2))/(Ly + y_calib) * fx1 
        
        phase_l = (phase_l - fs)
        
        
        
        phase_l = phase_l[:,y_calib:-y_calib]
        
        if idx == len(images_angles) -1 and plot_images:
            plt.figure()
            plt.title('phase at half hight x = (xmax - xmin)/2 and last angle ')
            plt.plot(phase_l[phase_l.shape[0]//2,:])
            plt.plot(phase_l[phase_l.shape[0]//2,::-1])
            plt.vlines([len(phase_l[0])//2], -1 , 1)
        
        phase_angles.append(phase_l)
    if plot_images or plot_phases:
        plt.show()
    
    
    return phase_angles
        

def reconstruct(images_angles, angles, Ly,center, y_calib = 200, slices = [1100], shift = 217, output = False):
    phase_angles = calculate_phase(images_angles, Ly, center = center , slices = slices, plot_images = True, plot_phases = True, shift = shift,y_calib = y_calib)
    
    
    
    if len(angles) >= 2 and angles[-1] != np.pi/2:
        phase_angles = np.array(list(phase_angles) + [np.fliplr(ph) for ph in phase_angles[::-1][:]])
        angles = list(angles) + list(np.pi - angles[::-1][:])
        
    elif len(angles) >= 2:
        phase_angles = np.array(list(phase_angles) + [np.fliplr(ph) for ph in phase_angles[::-1][1:]])
        angles = list(angles) + list(np.pi - angles[::-1][1:])
        
    phase_angles = np.array(phase_angles)
    angles = np.array(angles)

    
    if output:
        fig = plt.figure(figsize = (5,5))
        plt.title('Stack of Phases')
        plt.imshow(phase_angles[:, 0, :], cmap='Greys_r', aspect =1000/len(phase_angles))
        plt.colorbar()
        plt.show()  

    

    proj = tomopy.normalize(phase_angles, np.ones_like(phase_angles), np.zeros_like(phase_angles))

    #rot_center = tomopy.find_center(proj, angles, init = int(proj.shape[2]/2), ind=0, tol=0.5)
    #print ('Center of rotation: ', rot_center, int(proj.shape[2]/2))

    #np.save("Data/Interferometer/data.npy", {'projections': proj, 'angles': angles})
    #print ('saved')
    
    start = t.time()
    
    recon = tomopy.recon(proj, angles, center=int(proj.shape[2]/2), algorithm='mlem', ncore = 8)
    recon = tomopy.circ_mask(recon, axis=0, ratio=0.95)
        
    print ('Time for reconstruction: ',t.time() - start)
    

    return recon
