
from epics import PV, caput, caget
import time
import socket

Channels_Sign = {0 :'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F', 6:'G', 7:'H', 
                 8:'I', 9:'J', 10: 'L', 11:'M', 12:'N', 13:'O', 14:'P', 15:'Q'}
Channels_Time = {0 :'T', 1:'T', 2:'T', 3:'T', 4:'T', 5:'T', 6:'T', 7:'T',
                 8:'U', 9:'U', 10: 'U', 11:'U', 12:'U', 13:'U', 14:'U', 15:'U'}

'''
config file:
trigger_mode_CH0:       trigger mode of oszi in Channel 0 - 3(0: stop, 1:single, 2:normal)
trigger_level:          voltage level for trigger in (-.5 to .5 V in 12 bit)
delay:                  time to wait before trigger send

measure file: (measurements of channels are part of devices, i.e. FaradayCup, Laser)
time:                   time of tigger send
'''

default_config = {'trigger_level_CH0': 100,
              'trigger_mode':2,
              'delay': 0.0}

class DRS4:
    def __init__(self, config):
        caput('FIN-LSCP-DRS1:FLAG_GAIN',1)
        self.config = config
    
    
    @property    
    def config(self):
        config = {}
        config['trigger_mode'] = self.getTriggerMode()
        config['trigger_level_CH0'] = self.getTriggerLevel(0)
        config['delay'] = self.delay
        return config
    
    
    @config.setter
    def config(self, config):
        self.setTriggerMode(config['trigger_mode'])
        self.setTriggerLevel( 0, config['trigger_level_CH0'])
        self.delay = config['delay']
    
    
    def VoltageCalibration(self):
        caput('FIN-LSCP-DRS1:START_GAIN_CAL.PROC',1)
        time.sleep(1)
        
        
    def TimeCalibration(self):
        caput('FIN-LSCP-DRS1:START_TIME_CAL.PROC',1)
        time.sleep(1)
        
    def readChannel(self, num_c):
        signal = caget('FIN-LSCP-DRS1:DRS_WFMS.VAL'+Channels_Sign[num_c])
        time = caget('FIN-LSCP-DRS1:DRS_WFMS.VAL'+Channels_Time[num_c])
        return time, signal
    
    def setTriggerLevel(self, num_c, tlevel):
        caput('FIN-LSCP-DRS1:CH'+str(num_c)+'_TLEVEL', tlevel)
        caput ('FIN-LSCP-DRS1:CH'+str(num_c)+'_TLEVEL.PROC',1)
        
    def getTriggerLevel(self, num_c):
        return caget('FIN-LSCP-DRS1:CH'+str(num_c)+'_TLEVEL')
        
    def setTriggerMode(self, mode = 2):#0: stop, 1:single, 2:normal
        caput('FIN-LSCP-DRS1:TRIG_MODE',mode)
        
    def getTriggerMode(self):
        return caget('FIN-LSCP-DRS1:TRIG_MODE')
        