# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 11:10:06 2016

@author: Benedikt Hermann
"""

import numpy as np
import matplotlib.pyplot as plt

theta0 = 0./180 * np.pi
R0 = 1.0

theta1 = 40./180 * np.pi
R1 = 1.0

theta2 = 80./180 * np.pi
R2 = 1.0

z0 = R0 * np.exp(1j * theta0)
z1 = R1 * np.exp(1j * theta1)
z2 = R2 * np.exp(1j * theta2)

z = np.array([z0, z1, z2])

def fit_circle(x, y):
    
    norms = np.array(x)**2 + np.array(y)**2
    A = np.array([np.diff(x),np.diff(y)]).T
    b = (np.diff(norms))/2.
    
    if len(x) == 3:
        z = np.linalg.solve(A,b)
        return z
    
    return np.linalg.lstsq(A,b)[0]


e = fit_circle(z.real, z.imag)

def cor(R):
    theta= R*2*np.pi/360.
    return np.round((np.cos(theta)*e[0]-np.sin(theta)*e[1]),6)
    