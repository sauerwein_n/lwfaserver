# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 14:02:34 2016

@author: hermann_b
"""

import time
import serial
import numpy as np

# find reference mark of both sensors after reboot. Set reboot=True
# CH0: Rot. pos. (R)
# CH1: Lin. pos. (L)


#reboot=True
print_feedback=False #Smaract MCS Feedback 

ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=9600
)

ser.isOpen()


def cmd(inp,output=False):
    command=(":"+inp+"\n").encode("ascii")
    ser.write(command)
    time.sleep(0.1)
    out=""
    while ser.in_waiting > 0:
        out += ser.read(1).decode()
    
    if out != "":
        if print_feedback: print(">>" + out)
    else : print("no output recieved")
    if output==True: return out
    
def frmR(direction):
    if direction=="fwd": cmd("FRM0,0,60000,1")
    if direction=="bwd": cmd("FRM0,1,60000,1")

def frmL(direction):
    if direction=="fwd": cmd("FRM1,0,60000,1")
    if direction=="bwd": cmd("FRM1,1,60000,1")

def moverelR(angle):
    cmd("MAR0,"+str(int(angle*1000000))+",0,60000")
    
def moverelL(dist):
    cmd("MPR1,"+str(int(dist*1000000))+",60000")

def getposR():
    ret=np.round(int(cmd("GA0",output=True)[4:-3].replace(",",""))/1000000.,5)
    return ret

def getposL():
    ret=np.round(int(cmd("GP1", output=True)[4:-1])/1000000.,6)
    return ret

def maaR(angle):
    rev=0
    
    #print (angle)
    
    cmd("MAA0,"+str(int(angle*1000000))+","+str(rev)+",0")
    while cmd("GS0",output=True)[-2]!=str(3):
        #print(cmd("GS0",output=True)[-2])
        if cmd("GS0",output=True)[-2] == str(0):
            print ('rot. stage got stuck... continue')
            break
        
        time.sleep(0.1)
    angle_ac = getposR()
    #print (angle_ac)
    #while abs(angle_ac-angle)>0.01:
    #    time.sleep(0.1)
    #    angle_ac = getposR()
    #    #print (angle_ac)
        
        
def mpaL(pos):
    
    #print ('desired position: ',pos)
    
    
    cmd("MPA1,"+str(int(pos*1e6))+",0")    
#    while cmd("GS1",output=True)[-2]!=str(3):
#        time.sleep(0.2)
    while cmd("GS1",output=True)[-2]!=str(3):
        #print(cmd("GS0",output=True)[-2])
        if cmd("GS1",output=True)[-2] == str(0):
            print ('lin. stage got stuck... continue')
            break

def movetoR(angle, direction):
    #step function
    while(np.abs(getposR()-angle)>0.1):
        if direction=="fwd": moverelR(0.1)
        if direction=="bwd": moverelR(-0.1)
        print("R",getposR())
    
    moverelR(angle-getposR())
    time.sleep(0.25)
    print("R",getposR())    

def movetoL(pos, direction):
    #step function
    while(np.abs(getposL()-pos)>0.01):
        if direction=="fwd": moverelL(0.01)
        if direction=="bwd": moverelL(-0.01)
        print("L",getposL())
    
    moverelL(pos-getposL())
    time.sleep(0.25)
    print("L",getposL()) 


def init():
    cmd("SCLF0,500")#set close-loop frequency!!
    cmd("SCLF1,150") # do not change this. Time is not important
    
    cmd("SCLS0,100000000")
    cmd("SCLS1,100000000")
    print("find rot. ref.")
    moverelR(-1)
    time.sleep(1)
    
    frmR("bwd")
    
    while cmd("GS0",output=True)[-2]!=str(3):
        if cmd("GS0",output=True)[-2] == str(0):
            print ('reference not found, stage probably to far from reference ')
            break
        
        #print(cmd("GS0",output=True)[-2])
        time.sleep(1)
    #set limit for rot. Positioner (5,-200)
    cmd("SAL0,0, 0,"+str(270e6)+",0")
    
    
    print("R",getposR())
    print("rot. ref. done")
    print("find lin. ref.")
    
    moverelL(-0.1)
    time.sleep(3)
    frmL("fwd")
    
    while cmd("GS1",output=True)[-2]!=str(3):
        #print(cmd("GS0",output=True)[-2])
        time.sleep(1)
        if cmd("GS1",output=True)[-2] == str(0):
            print ('lin. stage got stuck... continue')
            break

    print("L",getposL())
    print("lin. ref. done")
    cmd("SPL1,"+str(-3e4)+","+str(3e6))
#if reboot: init()
#time.sleep(2)
#moverelR(-5)
#time.sleep(10)
#print("R",getposR())
#maaR(344.35)
#time.sleep(5)
#print("R",getposR())




