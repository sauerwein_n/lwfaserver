import numpy as np
import matplotlib.pyplot as plt

from Scripts.PlottingServers import Plot1DServer


'''
config file:

id1:                 int
list:                list
okasdasd:            boolean

measurement file:

value:               list of rands (length is id1)
random:              float64 random number
'''

default_config = {'id1': 5,
                  'list': [1,2,3,4],
                  'okasdasd': True}

class test_device1:
    def __init__(self, config):
        
        self.config = config
        
        self.plot_server = None
    
    @property
    def config(self):
        config = {}
        config['id1'] = self.id
        config['list'] = self.list
        config['okasdasd'] = self.ok
        return config
    
    @config.setter
    def config(self, config):
        self.id = config['id1']
        self.list = config['list']
        self.ok = config['okasdasd']
        
    def measure(self):
        measurement = {}
        measurement['value'] = np.random.rand(int(np.round(self.id,0)))
        measurement['random'] = np.random.rand()
        return measurement
    def plot_measure(self, measurement, config = None):
        if self.plot_server == None:
            self.plot_server = Plot1DServer({'title':'test_device1', 'xlabel':'num', 'ylabel': 'rand'})
        
        self.plot_server.update(range(len(measurement['value'])),measurement['value'])
        
    def spec_home(self):
        print ('this stuff will be homed')
        