import numpy as np
import matplotlib.pyplot as plt

from Scripts.PlottingServers import Plot1DServer

'''
config file:
channel:            channel on DRS4 where Cup is connected to
amplification:      factor amplification of signal before drs4
R:                  resistance of termination [Ohm]
n_calib:            number of point to avarge over to find zero
t1:                 bound1 of integral (before peak up)
t2:                 bound2 of integral (between peak up and down)
t3:                 bound3 of integral (after shorter peak down)

measurement file:
time:               time (datetime object)
sig_time:           time axis of signal
sig_uvolt:          voltage of signal [uV]
peak_uvolt:         negative peak voltage [uV]
charge:             charge [fC]
'''

default_config = {'channel':2,
               'R': 50.,
               'amplification': 316,
                 'n_calib': 100,
                  't1': 450.,
                  't2': 475.,
                  't3': 600.,
                 }

class FaradayCup:
    #mendatory functions
    def __init__(self, config):
        
        self.config = config
        self.plot_server = None
        
    @property
    def config(self):
        config = {}
        config['channel'] = self.channel
        config['amplification'] = self.amplification
        config['R'] = self.R
        config['n_calib'] = self.n_calib
        config['t1'] = self.t1
        config['t2'] = self.t2
        config['t3'] = self.t3
        return config
    
    @config.setter
    def config(self,config):
        self.channel = config['channel']
        self.R =  config['R']
        self.amplification = config['amplification']
        self.n_calib = config['n_calib']
        self.t1 = config['t1'] 
        self.t2 = config['t2']
        self.t3 = config['t3']
    
    def measure(self):
        import time as ttime
        
        ttime.sleep(0.4)
        from glob import glob
        file_name = glob('/mnt/lwfa-oszi/Autosave/C'+str(self.channel)+'*')[-1]
        
        sig = np.loadtxt(file_name, delimiter=',', skiprows=5)
        sig_time = sig[:,0] * 1e9
        sig_uvolt = sig[:,1] * 1e6
        
        import datetime
        time = datetime.datetime.now()
        measurement = {}
        
        v_calib = np.mean(sig_uvolt[:self.n_calib])
        
        sig_uvolt = sig_uvolt - v_calib
        
        te = sig_time[-1]
        
        charge_tot = self.integrate_charge(sig_time, sig_uvolt, sig_time[0], te)
        charge1 = self.integrate_charge(sig_time, sig_uvolt, self.t1, self.t2)
        charge2 = self.integrate_charge(sig_time, sig_uvolt, self.t2, self.t3)
        charge3 = self.integrate_charge(sig_time, sig_uvolt, self.t3, te)
        
        
        
        measurement = {'time': time,'sig_time': sig_time, 'sig_uvolt': sig_uvolt, 'peak_uvolt': np.min(sig_uvolt),'charge_tot': charge_tot, 'charge1': charge1, 'charge2': charge2,'charge3': charge3 }
        return measurement
    
    def plot_measure(self, measurement, config = None):
        if self.plot_server == None:
            self.plot_server = Plot1DServer({'title':'FaradayCup', 'xlabel':'Time [ns]', 'ylabel': 'Signal [uV]'})
        
        self.plot_server.update(measurement['sig_time'], measurement['sig_uvolt'])
        

    
        
    def integrate_charge(self, sig_time, sig_uvolt, timea, timeb):
        return np.trapz(sig_uvolt[np.logical_and(sig_time > timea, sig_time < timeb)], x = sig_time[np.logical_and(sig_time > timea, sig_time < timeb)]*1e-9)*1e-6/self.R/self.amplification*1e15

    