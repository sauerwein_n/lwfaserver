# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 10:46:54 2016

@author: hermann_b
"""
import numpy as np
import pypylon

import matplotlib.pyplot as plt

import Devices.InterferometerTools.routines as r
from Devices.CameraTools.binning import binArray

from Scripts.PlottingServers import Plot2DServer

import time as t
# camera setup


# optical parameters


##parameters for spectrometer that are fixed
#xmax=6.0
#xmin=2.6
#ymin=3
#ymax=12

'''
config file:
xmax:               maximal x in mm
xmin:               minimal x in mm
zmax:               maximal z in mm
zmin:               minimal z in mm
z0:                 offset of z to fit blade edge
x0:                 offset of x to fit blade edge
Ly:                 width to do tomography in mm
y_calib:            length to take for calibration left and right of image in mm
ExposureTimeAbs:    exposure time in us
BitsOfImage:        8 or 12 (8 if you want to do fast images, for tomography use 12!)
TriggerMode:        On/Off
Tomography:         boolean
take_data:          boolean
num_dir:            Number of directions (ignored if Tomography = False)
num_av:             Number of images at one angle (ignored if Tomography = False)
live_preview:       boolean whether to show the plots while measureing (Tomography)

measurement file:
time:               time (datetime object)
im1:                first image
im2:                second image
'''

default_config = inter_config = {'xmax':4.,
                    'xmin':0.,
                    'zmin':-7., 
                    'zmax':7.,
                    'x0': 4.,
                    'z0': 10.5,
                    'ExposureTimeAbs':35,
                    'BitsOfImage':12,
                    'TriggerMode':False,
                    'binning':10,
                    'Tomography':False,
                    'num_av': 9,
                    'num_dir': 2,
                    'take_data': True,
                    'Ly': 11.,
                    'x_tomo': 0.4,
                    'shift':209,
                    'y_calib': 1.,
                    'live_preview':True}

mode = {False: 'Off', True: 'On'}
modei = {'Off': False, 'On': True}

class Interferometer:
    #mendatory functions
    def __init__(self, config, BladePositioning, DelayGenerator):
        
        #fixed variables
        self.X_nozzle=1300
        self.psize=1./127.95 #mm
        self.mm=int(1./self.psize)
        self.ep=1.74/self.psize
        
        self.camera_serial_number = 21547524
        
        
        print ('connecting camera')
        self.connect_camera()
        print ('done')
        
        self.config = config
        
        self.blade = BladePositioning
        self.delay = DelayGenerator
        
        self.plot_server_im = None
        self.plot_server_ph = None
        self.plot_server_tomo = None
    
    @property
    def config(self):
        config = {}
        config['ExposureTimeAbs'] = self.cam.properties['ExposureTimeAbs']
        config['TriggerMode'] = modei[self.cam.properties['TriggerMode']]
        config['BitsOfImage'] = int(self.cam.properties['PixelFormat'][4:])
        config['binning'] = self.n_binning
        
        config['xmin'], config['xmax'],config['zmin'], config['zmax']= (self.xmin, self.xmax, self.zmin, self.zmax)
        config['x0'], config['z0'] = (self.x0, self.z0)
        config['Tomography'] = self.Tomography
        config['num_dir'] = self.num_dir
        config['num_av'] = self.num_av
        config['take_data'] = self.take_data
        config['Ly'] = self.Ly
        config['x_tomo'] = self.x_tomo
        config['shift'] = self.shift
        config['live_preview'] = self.live_preview
        config['y_calib'] = self.y_calib

        return config
    
    @config.setter
    def config(self,config):
        self.take_data = config['take_data']
        self.cam.properties['PixelFormat'] = 'Mono'+str(config['BitsOfImage'])
        self.cam.properties['ExposureTimeAbs']=config['ExposureTimeAbs']
        self.cam.properties['TriggerMode']=mode[config['TriggerMode']]
        self.n_binning = config['binning']
        
        
        self.xmin, self.xmax, self.zmin, self.zmax = (config['xmin'], config['xmax'],
                                                      config['zmin'], config['zmax'])
        self.x0, self.z0 = (config['x0'], config['z0'])
        
        self.set_frame(config['xmin'], config['xmax'],
                       config['zmin'], config['zmax'])
        
        self.Tomography = config['Tomography']
        self.num_dir = config['num_dir']
        self.num_av = config['num_av']
        self.Ly = config['Ly']
        self.x_tomo = config['x_tomo']
        self.shift = config['shift']
        self.live_preview = config['live_preview']
        self.y_calib = config['y_calib']

    
    def measure(self):
        import datetime
        time = datetime.datetime.now()
        
        
        
            
        if self.Tomography == False or self.blade == None or self.delay == None:
            
            D=[]
            for img in self.cam.grab_images(2):
                D.append(np.fliplr(img))
            
            measurement = {'time': time,'im1': D[0], 'im2': D[1]}
            return measurement
        
        else:
        
            if self.plot_server_tomo == None:
                self.plot_server_tomo = Plot2DServer({'title':'Tomography live', 'xlabel':'z [mm]', 'ylabel': 'x [mm]','image': True})
            
            blade_config0 = self.blade.config
            global t_begin
            t_begin = t.time()
            
            angles_deg = 90 * np.linspace(0, 1, self.num_dir) # np.round(90 *(np.cos(np.linspace(0, np.pi/2, self.num_dir)) - 1),3)#(np.cos(np.linspace(0, np.pi/2, self.num_dir)) - 1)
            
            if self.take_data:
                images_angles = self.tomography_take_data(blade_config0, angles_deg, save = True)
            
            else:
                dict_images_angles = np.load("Data/Interferometer/images_angles.npy").item()
                images_angles = dict_images_angles['images_angles']
                angles_deg = dict_images_angles['angles']
                del dict_images_angles
                
                self.save_phases(images_angles, angles_deg)
            
            res = update_tomography(self, images_angles,-angles_deg/360*2*np.pi)

            
            #plt.figure()
            #plt.imshow(res[0, :,:], cmap='binary')
            #plt.title(r'density in $10^{18} 1/cm^3$')
            #plt.colorbar()
            
            #plt.figure()
            #plt.plot(res[0,res.shape[1]//2, :])
            #plt.plot(res[0,:, res.shape[2]//2])
            #print ('bringing stage to original position')
            #self.blade.set_config(blade_config0)

            
            measurement = {'time': time, 'images_angles': images_angles, 'reconstruction': res}
            
            
            
            
            return measurement
                
                
            
            
            
            
        
    def plot_measure(self, measurement, config = None):
        if self.Tomography:
        
            if self.plot_server_tomo == None:
                self.plot_server_tomo = Plot2DServer({'title':'Tomography live', 'xlabel':'z [mm]', 'ylabel': 'x [mm]','image': True})
            
            im1 = binArray(measurement['reconstruction'][0, :,:], 0, self.n_binning, self.n_binning, np.sum)
            im1 = binArray(im1, 1, self.n_binning, self.n_binning, np.sum)
            
            self.plot_server_tomo.update(im1,extent=np.array([self.zmin, self.zmax, self.zmin, self.zmax])) 
            
            plt.figure()
            
            
            [plt.plot(measurement['reconstruction'][0, measurement['reconstruction'].shape[1]//2 + int(y/ self.psize),:]) for y in np.linspace(0,4,8)]
            plt.show()
            
            
            return 0
            #measurement['im1'] = measurement['images_angles'][0][0]
            #measurement['im2'] = measurement['images_angles'][0][1]
        
        
        if config != None:
            zmin = config['zmin']
            zmax = config['zmax']
            xmin = config['xmin']
            xmax = config['xmax']
            z0 = config['z0']
            x0 = config['x0']
        else:
            zmin = self.zmin
            zmax = self.zmax
            xmin = self.xmin
            xmax = self.xmax
            x0 = self.x0
            z0 = self.z0
        
        
        if self.plot_server_im == None:
            self.plot_server_im = Plot2DServer({'title':'Interferometer Image', 'xlabel':'z [mm]', 'ylabel': 'x [mm]','image': True})
        if self.plot_server_ph == None:
            self.plot_server_ph = Plot2DServer({'title':'Interferometer Phase', 'xlabel':'z [mm]', 'ylabel': 'x [mm]','image': True})    

        im1 = measurement['im1']
        im2 = measurement['im2']
        
        shape = measurement['im1'].shape
        
        im1bx1 = binArray(im1, 0, self.n_binning, self.n_binning, np.sum)
        im1bxy = binArray(im1, 1, self.n_binning, self.n_binning, np.sum)
        
        self.plot_server_im.update(np.flipud(im1bxy),extent=self.psize*np.array([0.,shape[1], 0., shape[0]])-np.array([z0, z0, x0,x0]))
        
        
        
        im1bx = binArray(im1[-self.x2p:-self.x1p,:], 0, self.n_binning, self.n_binning, np.sum)
        im2bx = binArray(im2[-self.x2p:-self.x1p,:], 0, self.n_binning, self.n_binning, np.sum)
        
        from Devices.InterferometerTools.tomography import calculate_phase
        
        phase = calculate_phase([[im1bx, im2bx]], int(self.Ly/self.psize),center = int(self.z0/self.psize),y_calib = int(self.y_calib/self.psize), plot_images = True, plot_phases = False,shift=self.shift)[0]
        
        print (phase)
        
        phase = binArray(phase, 1, self.n_binning, self.n_binning, np.sum)
        

        self.plot_server_ph.update(np.flipud(-phase), extent=[-self.Ly/2,self.Ly/2, xmin, xmax])
        
    def turn_off(self):
        self.cam.close()
    def spec_reconnect(self):
        self.cam.close()
        self.connect_camera()    
    #additional function
    def connect_camera(self):
        available_cameras = pypylon.factory.find_devices()
        
        if len(available_cameras) == 0:
            assert 'No cameras found'
            
        print ('available cameras:')
        for cam in available_cameras:
            print(cam)
            
        for availca in available_cameras:
            int(str(availca)[-10:-2])
            if int(str(availca)[-10:-2]) == self.camera_serial_number:
                self.cam_to_connect = availca
                cam = pypylon.factory.create_device(availca)
                print ('--> camera (',self.camera_serial_number,') connected')
           
        try:
            cam.open()
        except:
            assert('camera not connectable')
        
        
        #cam.properties['GevSCPSPacketSize']=9014
        cam.properties['AcquisitionFrameRateEnable']=False
        cam.properties['AcquisitionFrameRateAbs']=100
        cam.properties['GainRaw']=150
        

        self.cam = cam
    
    def set_frame(self, xmin, xmax, zmin, zmax):
        self.x2p=int((xmax + self.x0)/self.psize)
        self.x1p=int((xmin + self.x0)/self.psize)
        self.z1p=int((zmin + self.z0)/self.psize)
        self.z2p=int((zmax + self.z0)/self.psize)
        
    def Alpha(self, D1, D2):
        sigmax=5
        sigmay=0.5
        
        from scipy import ndimage
        
        #smoothing
        Ds1=ndimage.filters.gaussian_filter(np.array(D1,dtype=float), [sigmax, sigmay], mode='nearest', truncate=2)
        Ds2=ndimage.filters.gaussian_filter(np.array(D2,dtype=float), [sigmax, sigmay], mode='nearest', truncate=2)
        
        #calculate phase
        
        alpha=-r.unwrap(Ds1[self.x1p:self.x2p,:],Ds2[self.x1p:self.x2p,:])[:,self.z1p:self.z2p]
        
        return alpha
    
    def tomography_take_data(self, blade_config0, angles, save = True):
        images_angles = []
        global t_begin
        
        blade_config1 = blade_config0.copy()
        
        from Devices.InterferometerTools.tomography import reconstruct
        from multiprocessing import Process
        
        
        p = None
        
        for i ,angle in enumerate(angles):
            
            
            
            print ('Angle ',angle)
            blade_config1['pos_R'] = blade_config0['pos_R'] + angle
            self.blade.config = blade_config1
            print ('Time [Drive to angle ',angle,'] = ', t.time() - t_begin)
            if p:
                p.join()

            Ds = []

            for j in range(self.num_av):

                t.sleep(0.2)
                print ('Nummer ',j )
                self.delay.measure()
                D = []
                
                for img in self.cam.grab_images(2):
                    D.append(np.fliplr(img))
                Ds.append(D)

            images_angle = np.mean(np.array(Ds), axis = 0)
            
            images_angles.append(images_angle)
            
            if self.live_preview and i != len(angles)-1:
                p = Process(target = update_tomography, args = (self,images_angles,-angles[:i + 1]/360*2*np.pi))
                p.start()
            print ('Time [Take images at angle ',angle,'] = ', t.time() - t_begin)
            
            
        if save:    
            np.save("Data/Interferometer/images_angles.npy", {'images_angles': images_angles, 'angles': angles})
        
        
        self.blade.config = blade_config0
        
        print ('Time [Drive to angle 0] = ', t.time() - t_begin)
        
        return images_angles
    
    def save_phases(self, images_angles, angles):
        from Devices.InterferometerTools.tomography import calculate_phase
        phase_angles = calculate_phase(images_angles, int(self.Ly/self.psize), center = int(self.z0/self.psize) , plot_images = True, shift = self.shift,y_calib = int(self.y_calib/self.psize)) #1520
    
        del images_angles
    
        if len(angles) >= 2 and angles[-1] != np.pi/2:
            phase_angles = np.array(list(phase_angles) + [np.fliplr(ph) for ph in phase_angles[::-1][:]])
            angles = list(angles) + list(np.pi - angles[::-1][:])
        
        elif len(angles) >= 2:
            phase_angles = np.array(list(phase_angles) + [np.fliplr(ph) for ph in phase_angles[::-1][1:]])
            angles = list(angles) + list(np.pi - angles[::-1][1:])
        
        
        #save
        np.save("Data/Interferometer/phase_angles.npy", {'phase_angles': phase_angles, 'angles': angles})
                        
def update_tomography(self, images_angles, angles_rad):

    from Devices.InterferometerTools.tomography import reconstruct

    res = reconstruct(images_angles,angles_rad, int(self.Ly/self.psize),center =int(self.z0/self.psize),y_calib = int(self.y_calib/self.psize) , slices = [images_angles[0][0].shape[0] - int((self.x_tomo + self.x0)/self.psize)], shift = self.shift, output = self.live_preview)

    A_Ar=4.203e-6 # molar refractifity of Argon, used to go from phase to density
    NA= 6e23 #Avogadro const 1/mol
    lam= 632.8e-9 #wavelength laser HeNe


    res = -res*lam/(self.psize*1e-3)/(2.*np.pi)*2./3.*NA/A_Ar*1e-6*1e-18
    
    print ('about to update')
    
    im1 = binArray(res[0, :,:], 0, self.n_binning, self.n_binning, np.sum)
    im1 = binArray(im1, 1, self.n_binning, self.n_binning, np.sum)
    
    self.plot_server_tomo.update(im1,extent=np.array([self.zmin, self.zmax, self.zmin, self.zmax]))         
    
    print ('plot updated')
    
    return res
                        
                        
    

