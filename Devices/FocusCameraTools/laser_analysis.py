import numpy as np
import matplotlib.pyplot as plt

import skimage

psize=1.67

def px2um(xpx,ypx):
    #return: xmm, ymm
    
    xum = xpx * psize
    yum = ypx * psize
    
    return xum, yum

def um2px(xum,yum):
    #return: xpx, ypx
    
    xpx = np.round(xum / psize)
    ypx = np.round(yum / psize)
    
    return xpx, ypx

def showimage(im, axis = None):
    if axis == None:
        plt.figure()
        ax = plt.subplot(111)
    ny, nx = im.shape
    axis.imshow(im,extent = [0-0.5*psize,nx*psize -0.5*psize,ny*psize -0.5*psize,0-0.5*psize])#, vmin = 0, vmax = 2e-4)
    

def find_beam(im, plot = True, nFWHM = 2, axis = None):
    #returns: (Maximum intensity, Positionx (um), Positiony (um)), HalfMaximum contour of beam
    
    #ny, nx = im.shape 
    
    itmaxypx, itmaxxpx = np.unravel_index(im.argmax(), im.shape) 
    itmax = im[itmaxypx, itmaxxpx]
    
    itmaxxum, itmaxyum = px2um(itmaxxpx, itmaxypx)
    
    px_box = 100 


    im_reduced = im[max([itmaxypx-px_box,0]):min([itmaxypx+px_box, im.shape[0]]), max([itmaxxpx-px_box, 0]):min([itmaxxpx+px_box,im.shape[1]])]
    
    #filter 
    from skimage.filters import gaussian
    
    im_reduced = gaussian(im_reduced, sigma = 1)
    itmax = np.max(im_reduced)
    from skimage.measure import find_contours
    cs = find_contours(im_reduced, itmax/2)
    #print ('# of contours: ',len(cs))
    clens = [len(con[0]) for con in cs]
    arg = np.argmax(clens)
    c = cs[arg]
    
        
    cxpx = c.T[1] + itmaxxpx-px_box#+ itmaxxpx-framepx
    cypx = c.T[0] + itmaxypx-px_box#+ itmaxypx-framepx
    
    cxum, cyum = px2um(cxpx, cypx)
    
    
 
        
    
    return (itmax, itmaxxum, itmaxyum),(cxum, cyum)