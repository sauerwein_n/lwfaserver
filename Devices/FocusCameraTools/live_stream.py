# -*- coding: utf-8 -*-
"""
Created on Thu Feb  9 11:57:06 2017

@author: hermann_b
"""
import sys
sys.path.append('../../')

import Devices.FocusCameraTools.laser_analysis as LA

import pypylon
import matplotlib.pyplot as plt
import numpy as np

import skimage
#Camera properties
psize=1.67
#objects for camera for focus

nhist = 100
mode = 'live'

ref_pos = np.array([-18.78008460998535, 2.7302064895629883, 15.329874038696289])

xs = ref_pos[0]# + np.linspace(-1,0,10)
ys = ref_pos[1]# + np.linspace(-0.5,0.5,10)
zs = ref_pos[2]  + np.linspace(-2,0,20) 

XS, YS, ZS = np.meshgrid(xs, ys, zs)



positions = np.array([XS.flatten(), YS.flatten(), ZS.flatten()]).T
                    
path = 'Data/scan/1dz2002/'

available_cameras = pypylon.factory.find_devices()
print('Available cameras are', available_cameras)
for availca in available_cameras:
    if str(availca)[-3] == '8':
        cam = pypylon.factory.create_device(availca)
        print ('camera found =) Be happy')

try:
    cam
except:
    print ('camera not found')  
    sys.exit()

cam.open()
cam.properties['ExposureTimeAbs'] = 10000
#print(cam.properties.keys())
cam.properties['PixelFormat'] = 'Mono12'
#cam.properties['TriggerMode'] = 'Off'
cam.properties['Width'] = 2630
cam.properties['Height'] = 2578
cam.properties['OffsetX'] = 0
cam.properties['OffsetY'] = 170

def take_img(name = None,save = False, nav = 1):
    im = [image for image in cam.grab_images(nav)][0]
    
    if save:
        ims = skimage.img_as_float(im)
        im16 = skimage.img_as_uint(ims)
        from skimage.io import imsave
        imsave(path+name+".tif",im16)
    
    return im

def close(event):
    import sys
    sys.exit(1)    

    
global all_time_int  
global all_time_FWHMx  
global all_time_FWHMy  
global intensities
global FWHMxs
global FWHMys
    
all_time_int = 0
all_time_FWHMx = 0.007
all_time_FWHMy = 0.007  
    
    
intensities = [0]
FWHMxs = [0.007]
FWHMys = [0.007]
    
plt.ion()
fig_beam = plt.figure()
ax_beam = plt.subplot(111)
#from skimage.io import imread

fig_int = plt.figure()
ax_int = plt.subplot(121)
ax_FWHM = plt.subplot(122)

fig_beam.canvas.mpl_connect('close_event', close)
fig_int.canvas.mpl_connect('close_event', close)



def update_plots(im):
    global all_time_int  
    global all_time_FWHMx  
    global all_time_FWHMy
    global intensities
    global FWHMxs
    global FWHMys
    
    ax_beam.cla()
    ax_int.cla()
    ax_FWHM.cla()
    
    max_int = np.max(im)
    ymin = 0; ymax = psize*im.shape[0]*1e-3
    xmin = 0; xmax = psize*im.shape[1]*1e-3
    if max_int > 1000:
        maximum_pos, contour = LA.find_beam(im, axis = ax_beam)
        ymin, ymax = ax_beam.get_ylim()
        xmin, xmax = ax_beam.get_xlim()
        cxmm, cymm = contour
        FWHMx = np.max(cxmm) - np.min(cxmm)
        FWHMy = np.max(cymm) - np.min(cymm)
        if FWHMx < all_time_FWHMx:
            all_time_FWHMx = FWHMx        
        if FWHMy < all_time_FWHMy:
            all_time_FWHMy = FWHMy  
    else:
        ax_beam.set_ylim([ymin, ymax])
        ax_beam.set_xlim([xmin, xmax])
        LA.showimage(im, axis = ax_beam)
        print ('No beam detected')
        FWHMx = None
        FWHMy = None 
    intensities = np.append(intensities,max_int)
    FWHMxs = np.append(FWHMxs,FWHMx)
    FWHMys = np.append(FWHMys,FWHMy)
    if max_int > all_time_int:
        all_time_int = max_int
    if len(intensities) > nhist:
        ax_int.plot(intensities[-nhist+1:])
        ax_FWHM.plot(FWHMxs[-nhist+1:])
        ax_FWHM.plot(FWHMys[-nhist+1:])
    else:
        ax_int.plot(intensities)
        ax_FWHM.plot(FWHMxs)
        ax_FWHM.plot(FWHMys)
    ax_int.hlines((all_time_int),0,nhist, color = 'b')
    ax_FWHM.hlines((all_time_FWHMx),0,nhist, color = 'b')
    ax_FWHM.hlines((all_time_FWHMy),0,nhist, color = 'g')
    
    ax_int.relim()
    ax_FWHM.relim() 
    
    ax_int.autoscale_view()
    ax_FWHM.autoscale_view()
    
    plt.pause(0.001)
    plt.draw()
    
if mode == 'live':
    while True:
        im = take_img()
        update_plots(im)


        
from time import sleep
if mode == 'scan':
    from PeriscopeControl import periscope
    P = periscope()   
    
    #np.save(positions, path+'positions')
    for position in positions:
        x = position[0]
        y = position[1]
        z = position[2]
        P.mAbs(x,y,z,camera_sync = True)
        sleep(1)
        im = take_img(name = 'FocusScan'+str(x)+'_'+str(y)+'_'+str(z), save = True)
        update_plots(im)    
        for i in plt.get_fignums():
            plt.figure(i)
            plt.savefig(path+'FocusScan'+str(x)+'_'+str(y)+'_'+str(z)+'figure%d.png' % i)
                
                


