# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 14:02:34 2016

@author: hermann_b & sauerwein_n
"""
import numpy as np
import os

import Devices.BladePositioningTools.smaract as s
from Devices.BladePositioningTools.correction_factor import cor

'''
config file:
pos_R:              angle of rotational stage [degree]
pos_L:              radius of blade set by linear stage [mm]

measurement file (n.a)
'''

default_config = {'pos_R': 20, 'pos_L':0}

class BladePositioning:
    def __init__(self, config):
        
        self.config = config
    @property
    def config(self):
        config = {}
        config['pos_R'] = s.getposR()
        config['pos_L'] = s.getposL() - cor(s.getposR())
        return config
    
    @config.setter
    def config(self, config):
        s.maaR(config['pos_R'])
        s.mpaL(config['pos_L']+cor(s.getposR()))
        
        print ('cor: ', cor(s.getposR()))
        

    def spec_home(self):
        s.init()


