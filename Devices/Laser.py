import numpy as np
from epics import PV, caput, caget

from Scripts.PlottingServers import Plot1DServer
        

'''
config file:
max_angle:          position of rotation stage where energy is maximal [deg]
attenuation:        percent of attenuation of laser pulse [percent]
comp_lin:           position of linear stage in compressor [mm]
channel:            channel connected to DRS4


measurement file:
time:               time of measurement
intensity:          peak of signal on photodiode (mV)

'''

default_config = {'max_angle': -22.9,
                'attenuation': 100,
                'comp_lin': 38.020,
                'channel': 1}


class Laser:
    def __init__(self, config, DRS4):
        self.old_comp_lin = 0
        self.drs = DRS4
        self.config = config
        self.plot_server = None
        
    @property
    def config(self):
        config = {}
        config['max_angle'] = self.max_angle
        config['attenuation'] = self.get_attenuation()
        config['comp_lin'] = self.get_lin_comp()
        config['channel'] = self.channel
        return config    
    
    @config.setter  
    def config(self, config):
        self.max_angle = config['max_angle']
        self.set_attenuation(config['attenuation'])
        self.set_lin_comp(config['comp_lin'])
        self.channel = config['channel']
        
    def set_attenuation(self, percent):
        
        phi = np.arccos(percent/100)*90/np.pi + self.max_angle
        caput('F10HU-LMOT715:MOT.VAL', phi)
        
    def get_attenuation(self):
        
        phi = caget('F10HU-LMOT715:MOT.VAL')
        
        return 100 * np.cos((phi - self.max_angle)/90*np.pi)
    
    def set_lin_comp(self, pos):
        #print (self.old_comp_lin, pos)
        if self.old_comp_lin == pos:
            return
        caput('F10HU-LMOT705:MOT.VAL', pos)
        self.old_comp_lin = pos
        import time
        time.sleep(2)
        
    def get_lin_comp(self):
        return caget('F10HU-LMOT705:MOT.VAL')
    
    def measure(self):
        import time as tttime        
        tttime.sleep(0.24)
        import datetime
        time = datetime.datetime.now()
        sig_time, sig_uvolt = self.drs.readChannel(self.channel)
        return {'time': time,'sig_time':sig_time,'sig_uvolt': sig_uvolt, 'peak_uvolt': np.max(sig_uvolt)}
    
    def plot_measure(self, measurement, config = None):

        if self.plot_server == None:
            self.plot_server = Plot1DServer({'title':'Laser', 'xlabel':'Time [ns]', 'ylabel': 'Signal [uV]'})
        
        self.plot_server.update(measurement['sig_time'], measurement['sig_uvolt'])
    
    
        
        
        