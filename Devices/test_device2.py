import numpy as np
import matplotlib.pyplot as plt

from Scripts.PlottingServers import Plot2DServer


'''
config file:

id:                  int
cool:                boolean

measurement file:

value:               2D array of rands (100x100)

'''

default_config = {'id': 5,
                  'cool': True}

class test_device2:
    def __init__(self, config):
        
        self.config = config
        
        self.plot_server = None
    
    @property    
    def config(self):
        config = {}
        config['id'] = self.id
        config['cool'] = self.cool
        return config
    
    @config.setter
    def config(self, config):
        self.id = config['id']
        self.cool = config['cool']
        
    def measure(self):
        measurement = {}

        measurement['value'] = np.random.random((100,100))
        return measurement
    def plot_measure(self, measurement, config = None):
        if self.plot_server == None:
            self.plot_server = Plot2DServer({'title':'test_device2', 'xlabel':'x', 'ylabel': 'y', 'image':False})
            
        self.plot_server.update(measurement['value'], [0,1,0,1])