import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../pyAPT/')
from pyAPT import Z806

'''
config file:
pos_Pay:                  current position of motor Pay
pos_Paz:                  current position of motor Paz

home_on_connect:          boolean
n_av:                     number of focus measurements per iteration
maxiter:                  maximal number of iterations
minint:                   minimal intensity to search for beam
focus_min:                desired focus [um]

measurement file (n.a)
'''

default_config = {'pos_Pay': 3.99396444, 
                 'pos_Paz': 5.16051188,
                 'home_on_connect':True,
                 'n_av': 10,
                 'maxiter':20,
                 'focus_min': 10.5}

class ParabolicMirror:
    def __init__(self, config, FocusCamera, Periscope, Laser):
        
        self.focus = FocusCamera
        self.per = Periscope
        self.laser = Laser
        
        
        import time
        time.sleep(0.1)
        try:
            self.connect_motors()
            print ('Connnection with motors sucessful!')
        except:
            print ('Connection to motors failed. Close all programs that'+' use the motors or restart the controllers')
            raise
        
        if config['home_on_connect']:
            self.home()
            
        self.config = config
    
    @property
    def config(self):
        config = {}                          
        config['pos_Pay'] = float(self.Pay.position())
        config['pos_Paz'] = float(self.Paz.position())
        config['home_on_connect'] = self.home_on_connect
        config['focus_min'] = self.focus_min
        config['n_av'] = self.n_av
        config['maxiter'] = self.maxiter
        return config
    
    @config.setter
    def config(self,config):
        self.Pay.goto(config['pos_Pay'])
        self.Paz.goto(config['pos_Paz'])
        self.focus_min = config['focus_min']
        self.n_av = config['n_av']
        self.maxiter = config['maxiter']
        self.home_on_connect = config['home_on_connect']
   
    def connect_motors(self):

        self.Paz = Z806(83835804)
        self.Pay = Z806(83835879)
        self.motors = [self.Pay, self.Paz]
    def spec_home(self):
        self.home()    
        
    def home(self):
        for motor in self.motors:
            motor.home()
            
    def spec_align(self):
        self.optimize()
        
        
    def optimize(self, sleep_time = 0.2, output = True):
        #this variable is necessary to compensate the attenuation of the laser
        global intmult
        intmult = 1.
        
        wanted_focus_um = self.focus_min
        
        maxiter = self.maxiter
        n_av = self.n_av
        
        global reduce_int
        reduce_int = False
        def to_min():
            global intmult
            
            FWHMxs = []
            FWHMys = []
            maxints = []
            for i in range(n_av):
                measurement = self.focus.measure()
                FWHMxs.append(measurement['FWHMx'])
                FWHMys.append(measurement['FWHMy'])
                maxints.append(measurement['maxit'])
                
            if output:
                self.focus.plot_measure(measurement)
                
            del measurement
            if output:
                print ('FWHMxs: ',FWHMxs)
                print ('FWHMys: ',FWHMys)
                print ('maxints: ',maxints)
                print ('mean(maxits): ',np.mean(maxints))
                
            res = np.sqrt((np.array(FWHMxs)**2 + np.array(FWHMys)**2)/2 + (np.array(FWHMxs) - np.array(FWHMys)) **2)#*4096/np.array(maxints)*intmult
            mean, error = np.mean(res), np.std(res, ddof = -1)/np.sqrt(len(res))
            global reduce_int
            if np.mean(maxints) > 4e3:
                reduce_int = True
                print ('will be attenuated')
                
            if output:
                print ('mean: ',mean,'error: ', error)
                
            if mean < wanted_focus_um:
                if output:
                    print ('The optimization converged. Final value: ',mean)
                return
            
            return mean, error


        
        
        def change_parameter(y, z, zc):
            print ('Parameters: ',(y, z, zc))
            config = self.config
            
            config['pos_Pay'] = y
            config['pos_Paz'] = z

            self.config = config
            
            config_per = self.per.config
            
            config_per['deltaz_Cz'] = zc 
            #Cz.mAbs(zc)
            self.per.config = config_per
            return True
        
        def opt_f(x):
            y = x[0]
            z = x[1]
            zc = x[2]*30. 
            if output:
                print ('-------------------------------------------')
            change_parameter(y,z, zc)
            
            #wait for oszillation to decay
            import time
            time.sleep(sleep_time)
            
            return to_min()[0]
        
        config0 = self.config
        config_per0 = self.per.config
        config0_focus = self.focus.config
        config0_focus['measure_full']=True
        self.focus.config = config0_focus
        x0 = [config0['pos_Pay'],
              config0['pos_Paz'], 
              config_per0['deltaz_Cz']/30.]
        
        def after_it(res):
            print ('###################')
            print ('step done')
            global reduce_int, intmult
            if reduce_int:
                config_laser = self.laser.config
                config_laser['attenuation'] = config_laser['attenuation']/1.15
                self.laser.config = config_laser
                intmult *= 1.15
                reduce_int = False
                print ('laser attenuated')
            
        import scipy.optimize
        res = scipy.optimize.minimize(opt_f, x0, method='Nelder-Mead',callback = after_it, options={'disp': output,
                                                                          'initial_simplex': None, 
                                                                          'maxiter': maxiter, 
                                                                          'xatol': 0.00005, 
                                                                          'return_all': False, 
                                                                          'fatol': wanted_focus_um**2})
        
        opt_f(res.x)
        
        
        
        #import noisyopt
        #res = noisyopt.minimizeSPSA(opt_f, x0,bounds = [[0.,6.],[0.,6.],[0.,2.5]], c = 0.001,a = 0.001, niter=maxiter,paired = False, disp=False, callback = after_it)
            
        if output:
            print (res)
        return res
            
            
        
        