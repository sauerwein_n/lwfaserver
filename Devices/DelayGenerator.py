# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 17:33:30 2017

@author: nicks
"""

from instruments.srs import SRSDG645
import quantities as pq

'''
config file:
jet_triggered:      boolean, true if jet opens on fire
mode:               'one shot' or 'continuous'
rate:               float, maximal possible rate (single shot) or rate of trigger (continuous)
t_sleep:               time that the program sleeps until measurement finished

measurement file (n.a)
'''
default_config = {'jet_triggered': True,
                    'mode': 'single shot',
                    'rate': 10.,
                    't_sleep': 0.05}

class DelayGenerator:
    def __init__(self, config):
        self.ddg = SRSDG645.open_tcpip('172.21.11.156',5025)
        self.ddg.trigger_source = 3
        self.jet = self.ddg.Output(self.ddg,3)
        
        self.config = config

    
    @property
    def config(self):
        config = {}
        config['mode'] = self.getMode()
        config['jet_triggered'] = self.getJet()
        config['rate'] = self.getRate()
        config['t_sleep'] = self.t_sleep
        return config
    
    @config.setter
    def config(self, config):
        self.setMode(config['mode'])
        self.setJet(config['jet_triggered'])
        self.setRate(config['rate'])
        self.t_sleep = config['t_sleep']
        
    def setMode(self, mode):
        if mode == 'single shot':
            self.ddg.trigger_source = 3
            return
        if mode == 'continuous':
            self.ddg.trigger_source = 1
            return
        print ('mode: '+mode+' unknown')
        raise
        
    def getMode(self):
        mode = {3: 'single shot', 1: 'continuous'}
        return mode[self.ddg.trigger_source]
    
    def setRate(self, rate):
        self.ddg.holdoff = pq.sec*1/rate -  pq.sec*1e-3
        
    def getRate(self):
        return 1/float(self.ddg.holdoff)
        
    def singleMode(self):
        self.ddg.trigger_source = 3
        
    def setJet(self, On):
        if On:
            self.jet.level_amplitude = pq.volt*5
        else:           
            self.jet.level_amplitude = pq.volt*0.51
            
    def getJet(self):
        if self.jet.level_amplitude == pq.volt*5:
            return True
        else:
            return False
            
    def measure(self):
        import datetime
        import time
        
        from threading import Thread
        
        def trigger():
            time.sleep(self.t_sleep)
            self.ddg.trigger()
        
        t1 = Thread(target = trigger)
        
        t1.start()
        
        time = str(datetime.datetime.now()).replace(':','-').replace(' ','_')
        measurement = {'time': time}
        

        return measurement

