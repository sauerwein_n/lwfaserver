
import Devices.FocusCameraTools.laser_analysis as LA

import pypylon
import matplotlib.pyplot as plt
import numpy as np


from Devices.CameraTools.binning import binArray
from Scripts.PlottingServers import Plot2DServer

'''
config file:
ExposureTimeAbs:    exposure time in us
min_intensity:      minimal intensity over which contour of FWHM will be fitted
measure_full:       boolean to indicate if image should be analysed (FWHMs, contour, ...)


measurement file:
time:               time (datetime object)
im:                 image of camera
FWHMx:              FWHM of intensity in x
FWHMy:              FWHM of intensity in y
cxmm:               x coordinate of contour [mm]
cymm:               y coordinate of contour [mm]
maxit:              maximal intensity [0 - 4096]
'''

default_config = {'ExposureTimeAbs': 10000,
                   'camera_serial_number':22005848,
                   'min_intensity': 2000,
                   'measure_full' : True}


class FocusCamera:
    
    def __init__(self, config):
        

        self.psize=1.67 #um
        
        self.camera_serial_number = 22005848
        
        self.connect_camera()
        
        self.config = config
        
        self.plot_server_im = None
        
    @property    
    def config(self):
        config = {}
        config['ExposureTimeAbs'] = self.cam.properties['ExposureTimeAbs']
        config['min_intensity'] = self.min_intensity
        config['measure_full'] = self.measure_full
        return config    
    
    @config.setter
    def config(self,config):
        self.min_intensity = config['min_intensity']
        
        self.cam.properties['ExposureTimeAbs']=config['ExposureTimeAbs']
        self.min_intensity = config['min_intensity']
        self.measure_full = config['measure_full']
        

    
    def measure(self):
        import datetime
        time = datetime.datetime.now()
        
        im = self.cam.grab_image()
        measurement = {'time': time,'im': im}
        
        if self.measure_full:
            max_int = np.max(im)
            if max_int > self.min_intensity:
                maximum_pos, contour = LA.find_beam(im, plot = False)
                cxum, cyum = contour
                FWHMx = np.max(cxum) - np.min(cxum)
                FWHMy = np.max(cyum) - np.min(cyum) 
                comment = 'beam detected'
            else:
                comment = 'No beam detected'
                FWHMx = 100
                FWHMy = 100
                cxum = None
                cyum = None
            
            measurement['comment'], measurement['FWHMx'], measurement['FWHMy'], measurement['cxum'], measurement['cyum'] = (comment, FWHMx, FWHMy, cxum, cyum)
            measurement['maxit'] = max_int
            
        return measurement
    
    def plot_measure(self, measurement, config = None):
        
        if self.plot_server_im == None:
            self.plot_server_im = Plot2DServer({'title':'FocusCamera', 'xlabel':'x [um]', 'ylabel': 'y [um]', 'image': True})  

        im = measurement['im'].astype('float32')
        
        
        
        shape = im.shape
        
        nFWHM = 2.
        
        cxum = measurement['cxum']
        cyum = measurement['cyum']
        FWHMx = measurement['FWHMx']
        FWHMy = measurement['FWHMy']
        
        if type(cxum) != type(None):
            xminum, xmaxum = np.min(cxum) - nFWHM * FWHMx, np.max(cxum) + nFWHM * FWHMx
            yminum, ymaxum = np.min(cyum) - nFWHM * FWHMy, np.max(cyum) + nFWHM * FWHMy

            im = im[int(yminum/self.psize):int(ymaxum/self.psize),int(xminum/self.psize):int(xmaxum/self.psize)]
            
        else:
            xminum = 0
            yminum = 0
            xmaxum = shape[0] * self.psize
            ymaxum = shape[1] * self.psize
        
        import math
        #autobinning
        n_binning = math.ceil(((xmaxum - xminum)/self.psize)/500)
        
        im = binArray(im, 0, n_binning, n_binning, np.mean)
        im = binArray(im, 1, n_binning, n_binning, np.mean)
        
        self.plot_server_im.update(np.flipud(im),extent=(xminum,xmaxum, yminum, ymaxum))
        
        
    
    
    def connect_camera(self):
        available_cameras = pypylon.factory.find_devices()
        
        if len(available_cameras) == 0:
            assert 'No cameras found'
            
        print ('available cameras:')
        for cam in available_cameras:
            print(cam)
            
        for availca in available_cameras:
            int(str(availca)[-10:-2])
            if int(str(availca)[-10:-2]) == self.camera_serial_number:
                self.cam_to_connect = availca
                cam = pypylon.factory.create_device(availca)
                print ('--> camera (',self.camera_serial_number,') connected')
           
        try:
            cam.open()
        except:
            assert('camera not connectable')
        
        
        #cam.properties['GevSCPSPacketSize']=9015
        cam.properties['TriggerMode']='Off'
        cam.properties['PixelFormat'] = 'Mono12'
        cam.properties['AcquisitionFrameRateEnable']=False
        cam.properties['AcquisitionFrameRateAbs']=100
        cam.properties['GainRaw']=150
        
        #These parameters have to be set because of the shot camera
        # --> delete if new camera is used
        cam.properties['Width'] = 2630
        cam.properties['Height'] = 2578
        cam.properties['OffsetX'] = 0
        cam.properties['OffsetY'] = 170
        
        self.cam = cam
    
    def spec_reconnect(self):
        self.cam.close()
        self.connect_camera()
    
    def turn_off(self):
        self.cam.close()