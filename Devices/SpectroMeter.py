

import TMCL
import Pyro4
import time

from serial import Serial

import numpy as np

import time


import sys
sys.path.insert(0, '../Nick/spectroanalyser/scripts/')

import sympy as sym
import numpy as np
import matplotlib.pyplot as plt
sym.init_printing(use_latex='mathjax')

import Physics
import BeamShape

import LinearModel_Accelerator as LMA
import LinearModel_Elements as LME
import VariableSpectrometer as VS
import ChangeOfE0 as COE


'''
config file:
lattice_elements:       string of connected lattice elements (i.e. Q1Q2Q3D1). The order of the elements is irrelevant.
                        because at the experiment we will only place them in the normal order Q1Q2Q3D1

Q1_gradient:            gradient of Q1 magnet [T/m]
Q1_angle:               angle [deg] of b_2 in complex plane (two times the real rotation in the laboratory frame of reference) 

Q2_gradient:            gradient of Q2 magnet [T/m]
Q2_angle:               angle [deg] of b_2 in complex plane (two times the real rotation in the laboratory frame of reference)

Q3_gradient:            gradient of Q3 magnet [T/m]
Q3_angle:               angle [deg] of b_2 in complex plane (two times the real rotation in the laboratory frame of reference)

D1_field:            field of D1 magnet [mT]
D1_angle:               angle [deg] of b_1 in complex plane (the real rotation in the laboratory frame of reference)

d_source_Q1:            distance between jet and Q1
d_Q1_Q2:                distance between Q1 and Q2
d_Q2_Q3:                distance between Q2 and Q3
d_Q3_D1:                distance between Q3 and D1
d_D1_screen:            distance between D1 and screen

E0:                     reference energy [MeV]. The energy that will be used to set up the focusing and deflection conditions

deflection:             distance that particles at reference energy are shifted on screen due to dipole (the dipole will be set when special function spec_set_dispersion is started)
delfection_angle:       [deg] direction in which the electrons should be dispersed

measurement n.a
'''

default_config = {'lattice_elements': 'Q1Q2Q3D1',
                  'Q1_gradient': 0.,
                  'Q1_angle': 0.,
                  'Q2_gradient': 0.,
                  'Q2_angle': 0.,
                  'Q3_gradient': 0.,
                  'Q3_angle': 0.,
                  'D1_field': 0.,
                  'D1_angle': 0.,
                  'd_source_Q1': 0.15,
                 'd_Q1_Q2': 0.15,
                 'd_Q2_Q3': 0.15,
                 'd_Q3_D1': 0.15,
                 'd_D1_screen': 0.15,
                 'E0': 1.,
                 'deflection': 0.001,
                 'deflection_angle':0.}


L_Q1 = 0.08
L_Q2 = 0.08
L_Q3 = 0.08
L_D1 = 0.08



class SpectroMeter:
    
    def __init__(self, config):
        self.halbacharrays = {}
        
        
        self.config = config
        
    @property
    def config(self):
        config = {}
        config['lattice_elements'] = ''.join([key for key in self.halbacharrays.keys()])
        
        config['deflection'] = self.deflection
        config['deflection_angle'] = self.deflection_angle
        
        if 'Q1' in self.halbacharrays:
            Q1 = self.halbacharrays['Q1']
            
            Q1field = Q1.field
            config['Q1_gradient'] = np.abs(Q1field)
            config['Q1_angle'] = np.angle(Q1field)*180/np.pi
            
        else:
            config['Q1_gradient'] = 0.
            config['Q1_angle'] = 0.
        
        if 'Q2' in self.halbacharrays:
            Q2 = self.halbacharrays['Q2']
            
            Q2field = Q2.field
            config['Q2_gradient'] = np.abs(Q2field)
            config['Q2_angle'] = np.angle(Q2field)*180/np.pi
            
        else:
            config['Q2_gradient'] = 0.
            config['Q2_angle'] = 0.
            
        if 'Q3' in self.halbacharrays:
            Q3 = self.halbacharrays['Q3']
            
            Q3field = Q3.field
            config['Q3_gradient'] = np.abs(Q3field)
            config['Q3_angle'] = np.angle(Q3field)*180/np.pi
            
        else:
            config['Q3_gradient'] = 0.
            config['Q3_angle'] = 0.
            
        if 'D1' in self.halbacharrays:
            D1 = self.halbacharrays['D1']
            
            D1field = D1.field
            config['D1_field'] = np.abs(D1field)
            config['D1_angle'] = np.angle(D1field)*180/np.pi
            
        else:
            config['D1_field'] = 0.
            config['D1_angle'] = 0.
        
        config['d_source_Q1'] = self.d_source_Q1
        config['d_Q1_Q2'] = self.d_Q1_Q2
        config['d_Q2_Q3'] = self.d_Q2_Q3
        config['d_Q3_D1'] = self.d_Q3_D1
        config['d_D1_screen'] = self.d_D1_screen
        
        config['E0'] = self.E0
        return config
    
    @config.setter
    def config(self, config):
        
        
        lattice_elements = config['lattice_elements']
        
        
        # connect arrays
        for i in range(int(len(lattice_elements)/2)):
            name = lattice_elements[2*i:2*i+2]
            if name not in self.halbacharrays:
                self.halbacharrays[name] = HalbachArray()
                self.halbacharrays[name].connect_motors(name)
                
        if 'Q1' in self.halbacharrays:
            Q1 = self.halbacharrays['Q1']
            Q1.field = config['Q1_gradient'] * np.exp(1j*config['Q1_angle']/180*np.pi)
       
        if 'Q2' in self.halbacharrays:
            Q2 = self.halbacharrays['Q2']
            Q2.field = config['Q2_gradient'] * np.exp(1j*config['Q2_angle']/180*np.pi)
            
        if 'Q3' in self.halbacharrays:
            Q3 = self.halbacharrays['Q3']
            Q3.field = config['Q3_gradient'] * np.exp(1j*config['Q3_angle']/180*np.pi)
            
        if 'D1' in self.halbacharrays:
            D1 = self.halbacharrays['D1']
            D1.field = config['D1_field'] * np.exp(1j*config['D1_angle']/180*np.pi)
        
        self.d_source_Q1= config['d_source_Q1']
        self.d_Q1_Q2= config['d_Q1_Q2']
        self.d_Q2_Q3= config['d_Q2_Q3']
        self.d_Q3_D1= config['d_Q3_D1'] 
        self.d_D1_screen= config['d_D1_screen']
        
        self.E0 = config['E0']
        
        
        self.deflection = config['deflection']
        self.deflection_angle = config['deflection_angle']
        
    def spec_set_stigmatic_focus(self):
        
        L_qu = np.array([L_Q1, L_Q2, L_Q3])


        g1, g2 = sym.symbols('g1 g2')

        parameters = [g1, g2]

        list_of_mat = [LME.Drift(d_source_Q1),
                    LME.Quadrupole(L_qu[0],g1,0),
                    LME.Drift(d_Q1_Q2),
                    LME.Quadrupole(L_qu[1],g2,np.pi/2),
                    LME.Drift(d_Q2_Q3),
                    LME.Quadrupole(L_qu[2],g1,0),
                    LME.Drift(d_Q3_screen)]
        
        E0 = self.E0 #MeV
        acc = LMA.accelerator(E0, parameters, list_of_mat)
        
        config = VS.solve_focus(acc, [1.,1.]) # the initial value is the gradient in T/m
        
        if np.abs(config[0]) >  self.halbacharrays['Q1'].max_field or np.abs(config[0]) <  self.halbacharrays['Q1'].min_field:
            
            print ('ATTENTION')
            print ('g1 = '+str(config[0]))
            print ('g1 not in ['+str(self.halbacharrays['Q1'].min_field)+', '+str(self.halbacharrays['Q1'].max_field)+']')
        
        if np.abs(config[1]) >  self.halbacharrays['Q2'].max_field or np.abs(config[1]) <  self.halbacharrays['Q2'].min_field:
            
            print ('ATTENTION')
            print ('g2 = '+str(config[1]))
            print ('g2 not in ['+str(self.halbacharrays['Q2'].min_field)+', '+str(self.halbacharrays['Q2'].max_field)+']')
            
            
        if np.abs(config[0]) >  self.halbacharrays['Q3'].max_field or np.abs(config[1]) <  self.halbacharrays['Q3'].min_field:
            
            print ('ATTENTION')
            print ('g1 = '+str(config[0]))
            print ('g1 not in ['+str(self.halbacharrays['Q3'].min_field)+', '+str(self.halbacharrays['Q3'].max_field)+']')
        
        
        self.halbacharrays['Q1'].field = config[0]
        self.halbacharrays['Q2'].field = 1j*config[1]
        self.halbacharrays['Q3'].field = config[0]
        
    def spec_set_deflection(self):
        
        
        def to_min(B):
            R = COE.Rf(self.E0, B)
            
            h1 = R * (1 - np.sqrt(1 - (L_D1/R)**2))
            h2 = L_D1 * self.d_D1_screen / R / np.sqrt(1 - (L_D1/R)**2)
            
            return (self.deflection - h1 - h2)**2
        
        from scipy.optimize import minimize_scalar
        
        B = minimize_scalar(to_min , bounds = [0.000001,COE.Bf(self.E0, L_D1)], method = 'Bounded').x
        
        if np.abs(B) >  self.halbacharrays['D1'].max_field or np.abs(B) <  self.halbacharrays['D1'].min_field:
            
            print ('ATTENTION')
            print ('B = '+str(B))
            print ('B not in ['+str(self.halbacharrays['D1'].min_field)+', '+str(self.halbacharrays['D1'].max_field)+']')
        
        self.halbacharrays['D1'].field = B * np.exp(1j*self.deflection_angle/180*np.pi)
        
    def spec_calibrate(self):
        
        for name, array in self.halbacharrays.items():
            array.calibrate()
            
    def spec_home(self):
        
        for name, array in self.halbacharrays.items():
            array.home()
            

@Pyro4.expose
class HalbachArray:
    
    def __init__(self):
        pass
    
    def connect_motors(self, name): 
        
        coefs_dphi = np.load('../Nick/rotating_coil/'+name+'_coef_phi.npy', encoding = 'latin1').item()
        self.name = name
        self.id = int(name[1]) + {'Q': 0, 'D': 3}[name[0]]
        self.mag_type = {'D':0, 'Q':1}[name[0]]  #mag_type: 0=dipole, 1=quarupole
        
        self.coefs = np.array(coefs_dphi['coefs']) #this is a dictionary with: phi, and coefs as entries 
        self.dphis = coefs_dphi['phi']
        
        
        self.inner = HalbachAxis()
        self.outer = HalbachAxis()
        
        self.outer.connect(2*self.id-1, 'l')
        self.inner.connect(2*self.id, 'r')
        
        self.set_field_parameters()
        
    def set_field_parameters(self):
        
        
        
        if self.name == 'Q1':
            self.rotating_coil_calibration = 6.36392067406e-05 / L_Q1 #[T/m/U]
            
        if self.name == 'Q2':
            self.rotating_coil_calibration = 5.77530976052e-05 / L_Q2 #[T/m/U]
            
        if self.name == 'Q3':
            self.rotating_coil_calibration = (6.36392067406e-05 + 5.77530976052e-05) / 2 / L_Q3 #[T/m/U]
            
            
        if self.name == 'D1':
            self.rotating_coil_calibration = 9.35305665612e-06 #[T/U]
        
        self.max_field = np.max(np.abs(self.coefs[:,self.mag_type])) * self.rotating_coil_calibration
        self.min_field = np.min(np.abs(self.coefs[:,self.mag_type])) * self.rotating_coil_calibration
        
        from scipy.interpolate import interp1d
        self.coefs_dphi_f = interp1d(self.dphis, np.hstack([self.coefs.T[:,1:],np.array([self.coefs.T[:,1]]).T]))

        self.coefs_dphi_f_per = lambda dphi: self.coefs_dphi_f(dphi%360 - 360)
    
    def home(self):
        self.inner.home()
        self.outer.home()
        
    def calibrate(self):
        self.inner.calibrate()
        self.outer.calibrate()
    
    def mRel(self, dphii, dphio):
        
        self.inner.mRel(phii)
        self.outer.mRel(phio)
        
    def mAbs(self, phii, phio):
        
        self.inner.mAbs(phii)
        self.outer.mAbs(phio)
        
    @property
    def field(self):
        phii = self.inner.getPos()
        phio = self.outer.getPos()
        
        dphi = phio - phii
        
        return self.coefs_dphi_f_per(dphi)[self.mag_type] * np.exp(1j * phii / 180 * np.pi) * self.rotating_coil_calibration
        
        
    @field.setter
    def field(self, field):
        coef_wanted = np.abs(field)/self.rotating_coil_calibration
        angle_wanted = np.angle(field) * 180/np.pi
        
        from scipy.optimize import minimize_scalar

        def to_min(dphi, coef_wanted):
    
            return (np.abs(self.coefs_dphi_f_per(dphi)[self.mag_type]) - coef_wanted)**2


        dphi_wanted = minimize_scalar(to_min, args = (coef_wanted)).x

        angle_coef = np.angle(self.coefs_dphi_f_per(dphi_wanted)[self.mag_type])*180/np.pi
        #print (angle_coef)
        phi_tot_wanted = (angle_wanted - angle_coef)/(self.mag_type + 1)

        #print ('phii: ',phi_tot_wanted, ' ;phio: ',phi_tot_wanted + dphi_wanted)
        
        phii = phi_tot_wanted
        phio = phi_tot_wanted + dphi_wanted
        
        self.mAbs(phii, phio)

    

@Pyro4.expose
class HalbachAxis:
    def __init__(self):
        
        self.timeout = 120 #sec
        
        

    def connect(self, mid, orientation, port = "/dev/ttyUSB1", steps_per_turn = None):
        
        self.serial_port = Serial(port, timeout=2)
        self.bus = TMCL.connect(self.serial_port)
        
        
        self.mid = mid
        
        self.motor = self.bus.get_motor(self.mid)
        
        self.orientation = orientation
        print ('Motor ('+orientation+') connected')
        
        self.set_motor_parameters()
        
        print ('Motor parameters set')
        
        self.steps_per_turn = 6559374
        if steps_per_turn:
            self.sets_per_turn = steps_per_turn
            
    def home(self):
        #find ref.
        self.motor.send(13, 0, 0, 0)
        t0 = time.time()
        while self.motor.send(13, 2, 0, 0).value:
            time.sleep(0.1)
            if (time.time() - t0) > self.timeout:
                self.stop()
                raise ValueError('Timeout reached in '+str(self.timeout)+' sec')
            
        
    def calibrate(self):

        self.home()
        
        if self.orientation == 'r':
            self.mAbs(300)
        
        else:
            self.mAbs(-300)
        
        
        self.home()
            
        self.steps_per_turn = abs(self.motor.axis.get(197))
        
    def set_motor_parameters(self):
        
        #ref speed
        self.motor.axis.set(194, 2047)
        self.motor.axis.set(195, 500)

        #pos. speed
        self.motor.axis.set(4,2047)

        #set standby current
        self.motor.axis.set(7,50)
        
        #end switch disable
        self.motor.axis.set(12, 0)
        self.motor.axis.set(13, 0)
        self.motor.send(9,79,0, 1)

        #Set home mode
        if self.orientation == 'r':
            self.motor.axis.set(193, 8 + 128)
            
        elif self.orientation == 'l':
            self.motor.axis.set(193, 7 + 128)
        else:
            raise ValueError('orientation has to be l or r, but '+str(self.orientation)+' was set.')

        #active stall filter
        self.motor.axis.set(173, 0)
        
    def mAbs(self, phi_deg, debug_torque = False):
        
        phi_current = self.getPos()
        phi_wanted = phi_deg
        
        if self.orientation == 'l':
            dphi = (phi_wanted - phi_current)%360 - 360
        else:
            dphi = (phi_wanted - phi_current)%360 
            
        return self.mRel(dphi, debug_torque = debug_torque)
        #self.motor.send(4, 0, 0, int(phi_deg / 360 * self.steps_per_turn))
        
        
        
        #while self.getSpeed():
        #    time.sleep(0.1)
        #    if (time.time() - t0) > self.timeout:
        #        self.stop()
        #        raise ValueError('Timeout reached in '+str(self.timeout)' sec')
                
    def mRel(self, dphi_deg, debug_torque = False):
        
        if self.orientation == 'l':
            dphi_deg = dphi_deg%360 - 360
        else:
            dphi_deg = dphi_deg%360
        
        self.motor.send(4, 1, 0, int(dphi_deg / 360 * self.steps_per_turn))
        
        if debug_torque:
            torques = []
            poss = []
            
        
        t0 = time.time()
        while self.getSpeed():
            time.sleep(0.1)
            if debug_torque:
                torques += [self.getLoad()]
                poss += [self.getPos()]
            
            if (time.time() - t0) > self.timeout:
                self.stop()
                raise ValueError('Timeout reached in '+str(self.timeout)+' sec')
                
        if debug_torque:
            return poss, torques
                
    def stop(self):
        self.motor.stop()
        
    def getPos(self):
        
        return (self.motor.axis.get(1) * 360 / self.steps_per_turn)%360
        
    def getLoad(self):
        
        return self.motor.axis.get(206)
        
    def getSpeed(self):
        
        return self.motor.axis.get(3)
        
    
#daemon = Pyro4.Daemon(host = "localhost", port = 9091)      # make a Pyro daemon
#ns = Pyro4.locateNS()                  # find the name server
#uri = daemon.register(HalbachAxis)   # register the greeting maker as a Pyro object
#ns.register("HalbachAxis", uri)   # register the object with a name in the name server

#print("Ready.")
#daemon.requestLoop() 
                                                                       
