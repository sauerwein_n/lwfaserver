# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 11:36:05 2017

@author: hermann_b-adm
"""
from Devices.DRS4 import DRS4

import numpy as np
import matplotlib.pyplot as plt

drs = DRS4(())

plt.ion()
while True:
    time , signal = drs.readChannel(0)
    plt.figure('hu')
    plt.clf()
    plt.plot(time, signal)
    plt.draw()
    plt.pause(0.001)