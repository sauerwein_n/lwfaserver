import numpy as np
import pypylon

import matplotlib.pyplot as plt

import Devices.InterferometerTools.routines as r


from Devices.CameraTools.binning import binArray
from Scripts.PlottingServers import Plot2DServer

# optical parameters


'''
config file:

y0:                 y offset in mm
z0:                 z offset in mm

ExposureTimeAbs:    exposure time in us
TriggerMode:        On/Off

measurement file:
time:               time (datetime object)
im1:                first image
'''

default_config = {
    'y0': 6.8,
    'z0': 8.45,
    'binning': 10,
    'calibrated':False,
    'ExposureTimeAbs':300,
    'TriggerMode':False}


mode = {False: 'Off', True: 'On'}
modei = {'Off': False, 'On': True}
class PlasmaCamHorizontal:
    #mendatory functions
    def __init__(self, config):
        
        self.psize=1./127.95*3/1.70 #mm
        self.ep=1.74/self.psize
        
        self.camera_serial_number = 21145133
        
        self.connect_camera()
        
        self.config = config
        
        self.plot_server_im = None
    
    
    @property
    def config(self):
        config = {}
        config['ExposureTimeAbs'] = self.cam.properties['ExposureTimeAbs']
        config['TriggerMode'] = modei[self.cam.properties['TriggerMode']]
        config['y0'] = self.y0
        config['z0'] = self.z0
        config['binning'] = self.n_binning
        config['calibrated'] = self.calibrated
        return config
    
    @config.setter
    def config(self,config):
        self.cam.properties['ExposureTimeAbs']=config['ExposureTimeAbs']
        self.cam.properties['TriggerMode']=mode[config['TriggerMode']]
        self.y0 = config['y0']
        self.z0 = config['z0']
        self.n_binning = config['binning']
        self.calibrated = config['calibrated']


    
    def measure(self):
        import datetime
        time = datetime.datetime.now()
        
        D=[]
        
        if self.calibrated:
            for img in self.cam.grab_images(2):
                D.append(np.fliplr(img))
            measurement = {'time': time,'im': D[0], 'im_c': D[1]}
            return measurement
        
        
        for img in self.cam.grab_images(1):
            D.append(np.fliplr(img))
        
        measurement = {'time': time,'im': D[0]}
        return measurement
        
    def plot_measure(self, measurement, config = None):

        if self.plot_server_im == None:
            self.plot_server_im = Plot2DServer({'title':'PlasmaCamHorizontal', 'xlabel':'z [mm]', 'ylabel': 'y [mm]', 'image': True})  

        im1 = measurement['im'].astype('float32')
        
        try:
            im_c = measurement['im_c'].astype('float32')
        
            im1 = im1 - im_c
        except:
            print ('not calibrated')
        shape = im1.shape
        
        im1 = binArray(im1, 0, self.n_binning, self.n_binning, np.mean)
        im1 = binArray(im1, 1, self.n_binning, self.n_binning, np.mean)
        
        self.plot_server_im.update(np.flipud(im1),extent=self.psize*np.array([0.,shape[1], 0., shape[0]]) - np.array([self.z0, self.z0, self.y0, self.y0]))
    
    def spec_reconnect(self):
        self.cam.close()
        self.connect_camera()    
    
    def turn_off(self):
        self.cam.close()
        
    #additional function
    def connect_camera(self):
        available_cameras = pypylon.factory.find_devices()
        
        if len(available_cameras) == 0:
            assert 'No cameras found'
            
        print ('available cameras:')
        for cam in available_cameras:
            print(cam)
            
        for availca in available_cameras:
            int(str(availca)[-10:-2])
            if int(str(availca)[-10:-2]) == self.camera_serial_number:
                self.cam_to_connect = availca
                cam = pypylon.factory.create_device(availca)
                print ('--> camera (',self.camera_serial_number,') connected')
           
        try:
            cam.open()
        except:
            assert('camera not connectable')
        
        
        #cam.properties['GevSCPSPacketSize']=9014
        
        self.cam = cam