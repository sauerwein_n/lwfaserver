from IPython.display import display_html

def hide():

    display_html('''<script>
    code_show=false; 
    function code_toggle() {
     if (code_show){
     $('div.input').hide();
     } else {
     $('div.input').show();
     }
     code_show = !code_show
    } 
    $( document ).ready(code_toggle);
    </script>
    <h1><a href="javascript:code_toggle()">Hide Code</a></h1>''')
    
def open_tab(url):
    print (url)
    display_html("<script type='text/javascript'>window.open('"+url+"', '_blank');</script>", raw = True)
    
def open_window(url, width = 800, height = 500):
    print (url)
    display_html("<script type='text/javascript'>window.open('"+url+"', '_blank','width="+str(width)+", height="+str(height)+"');</script>", raw = True)
    