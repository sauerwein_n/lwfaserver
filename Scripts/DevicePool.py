import glob
import os
import sys
import imp
import collections
import datetime
import numpy as np
import inspect

import collections

def import_(filename):
    path, name = os.path.split(filename)
    name, ext = os.path.splitext(name)

    file, filename, data = imp.find_module(name, [path])
    mod = imp.load_module(name, file, filename, data)

    return mod, name

class Device:
    def __init__(self, name, module):
        self.name = name
        self.module = module
        self.default_config = self.module.default_config
        self.config_temp = self.default_config
        
        self.connected = False
        
        #parameters that tell if the devices will me measured when the device_pool.measure and device_pool.plot_measure functions are run
        
        self.make_measurement = False 
        self.make_plot = False
        
        self.measureable = hasattr(getattr(self.module, self.name), 'measure')
        self.plotable = hasattr(getattr(self.module, self.name), 'plot_measure')
        
        self.spec_funcs = [name for name in getattr(self.module, self.name).__dict__.keys() if (len(name) > 4) & (name[:4] == 'spec' )]
        self.obj = None
        
    @property
    def config(self):
        if self.connected:
            return self.obj.config
        else:
            return self.config_temp
        
    @config.setter
    def config(self, config):
        if self.connected:
            self.obj.config = config
        else:
            self.config_temp = config
            
    def __str__(self):
        string = 'Device '+self.name+': ['
        string += 'connected: '+str(self.connected)
        string += ', measureable: '+str(self.measureable)
        string += ', plotable: '+str(self.plotable)
        string += ', make_measurement: '+str(self.make_measurement)
        string += ', make_plot: '+str(self.make_plot) + ']'
        
        return string
    
    def __print__(self):
        
        return str(self)

    
    def measure(self):
        
        assert (self.connected)
        assert (self.measureable)
        
        return self.obj.measure()
    
    def plot_measure(self, measurement):
        
        assert (self.connected)
        assert (self.measureable)
        assert (self.plotable)
        
        self.obj.plot_measure(measurement)
        
        


class DevicePool:
    
    def __init__(self, folder_autosave = 'Data/'):
        self.devices = {}
        self.import_devices()
        self.folder_autosave = folder_autosave
        
    @property
    def device_names(self):
        return [key for key in self.device_modules.keys()]
    
    @property
    def devices_connected(self):
        
        return [device for name, device in sorted(self.devices.items()) if device.connected]
    
    @property
    def devices_to_measure(self):
        
        return [device for name, device in sorted(self.devices.items()) if device.connected & device.make_measurement]
    
    @property
    def devices_to_plot(self):
        
        return [device for name, device in sorted(self.devices.items()) if device.connected & device.make_measurement & device.make_plot]
    
    @property
    def plotable_devices(self):
        return [device for name, device in sorted(self.devices.items()) if device.measureable]
    
    @property
    def measureable_devices(self):
        return [device for name, device in sorted(self.devices.items()) if device.plotable]
    
    def restorestate(self):
        state = np.load(self.folder_autosave+'last_device_pool_state.npy').item()
        
        for device_name, device in self.devices.items():
            device.config = state['config'][device_name]
        
        devices_to_connect = state['connected']
        
        while len(devices_to_connect) > 0:
            print (devices_to_connect)
            device_name = devices_to_connect.pop()
            device = self[device_name]
            connection_success = self.connect(device_name, savestate = False)
            if not connection_success:
                devices_to_connect = [device_name]+ devices_to_connect
                continue
            if device_name in state['to_measure']:
                device.make_measurement = True
            if device_name in state['to_plot']:
                device.make_plot = True
        
    def savestate(self):
        state = {}
        
        state['connected'] = [d.name for d in self.devices_connected]
        state['to_measure'] = [d.name for d in self.devices_to_measure]
        state['to_plot'] = [d.name for d in self.devices_to_plot]
        
        state['config'] = self.config
        
        np.save(self.folder_autosave+'last_device_pool_state.npy', state)
        
    def import_devices(self):
    
        device_scripts = glob.glob(os.getcwd()+'/Devices/*.py')
    
        device_modules = {}
    
        for device_script in device_scripts:

            mod, name = import_(device_script)
            if name == '__init__':
                continue
            
            device_modules[name] = mod
        
        self.device_modules = device_modules
        
        for name, mod in self.device_modules.items():
            self[name] = Device(name, mod)
        
    def connect(self, device_name, savestate = True):
        
        obj = getattr(self[device_name].module, self[device_name].name)
        needed_devices = np.array(inspect.getargspec(obj.__init__)[0])[2:]
        
        print ('device: ',self[device_name].name, '    needed devices: ',needed_devices)
        
        for needed_device in needed_devices:
            if not self[needed_device].connected:
                print (needed_device,' not yet all connected')
                return False
        
        
        self[device_name].obj = obj(self[device_name].config_temp, *[self[dev].obj for dev in needed_devices])
        self[device_name].connected = True
        
        if savestate:
            self.savestate()
        
        return True
        
    def __getitem__(self, device_name):
        return self.devices[device_name]
    
    def __setitem__(self, device_name, device):
        self.devices[device_name] = device
        
    def __str__(self):
        
        string = '['
        
        
        for name, device in self.devices.items():
            string += str(device) + ' \n '
            
        
        string += ']'
        
        return string
    
    def __print__(self):
        
        return str(self)
    
    @property
    def config(self):        
        time = str(datetime.datetime.now()).replace(':','-').replace(' ','_')
        config = {'time': time,
                  'devices_connected': [device.name for device in self.devices_connected],
                  'devices_to_measure': [device.name for device in self.devices_to_measure],
                  'devices_to_plot': [device.name for device in self.devices_to_plot]}

        for device_name, device in self.devices.items():
            import time
            t = time.time()
            config[device_name] = device.config
            print (device_name,' config time: ',time.time() - t)
        return config

    @config.setter
    def config(self, config):
        for device_name, device in self.devices.items():
            device.config = config[device_name]
            
        self.savestate()
            

        

        
        
    
    
    



  