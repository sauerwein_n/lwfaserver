
from threading import Thread
from multiprocessing import Pipe
import time
import datetime
import os

import numpy as np
from Scripts.PlottingServers import Plot1DServer 

#this class deals with measurements
class MeasurementManager:
    
    def __init__(self, device_pool, io):
        self.device_pool = device_pool
        self.io = io
        self.plot_hist = {}
        self.plot_servers = {}
    
        
    def measure(self, save = False, comment = '', plot_config = {}, name = None):
        print ('')
        print ('Measurement: '+comment)
        
        t0 = time.time()
        
        devices_to_measure = self.device_pool.devices_to_measure
        
        device_names = [device.name for device in devices_to_measure]
        
        measure_pipes = {device_name: Pipe() for device_name in device_names}
        
        
        device_measurers = {device_name: DeviceMeasurer(measure_pipes[device_name][0], self.device_pool[device_name]) for device_name in device_names}
        
        
        
        #start measurement
        for device_name in device_names:
            device_measurers[device_name].start()
        
        
        
        measurement = {'comment': comment, 'time': str(datetime.datetime.now()).replace(':','-').replace(' ','_')}
        measurement['config'] = self.device_pool.config
        
        measurement['measure'] = {}
        
        #get results
        time_out = 10
        t = time.time()
        
        while len(measurement['measure']) < len(devices_to_measure):
            
            if (time.time() - t) > time_out:
                raise ValueError("Time out: Some device seems not to answer. Probably an error in the code or the tigger didn't come")
            for device_name in device_names:
                if measure_pipes[device_name][1].poll():
                    
                    measurement['measure'][device_name] = measure_pipes[device_name][1].recv()
                    
                    if self.device_pool[device_name].make_plot:
                        
                        # here the way to plot is defined
                        self._plot_measure(measurement['measure'][device_name], 
                                            self.device_pool[device_name],
                                            plot_config)
                        
                        
                    
                    print (device_name+' measurement time: '+str(time.time() - t0))
        #stop measurement
        for device_name in device_names:
            device_measurers[device_name].join()
        
        if save:
            self.io.save(measurement, 
                         kind = 'measure', comment = comment, name = name)
        
        return measurement

    def _plot_measure(self, device_measurement, device, plot_config):
        if device.name in plot_config:
            if not device.name in self.plot_hist:
                self.plot_hist[device.name] = {}
            if not device.name in self.plot_servers:
                self.plot_servers[device.name] = {}
                
            parameters = plot_config[device.name]
            
            for para, to_plot in parameters.items():
                if para == 'plot_measure':
                    device.plot_measure(device_measurement)
                    continue
                
                if to_plot:
                    if para not in self.plot_servers[device.name]:
                        self.plot_servers[device.name][para] = Plot1DServer({'xlabel' : 'last measurements',
                                                                             'ylabel' : 'value', 
                                                                             'title': 'history: '+device.name+' '+para})

                    if para not in self.plot_hist[device.name]:
                        self.plot_hist[device.name][para] = [] 
                     
                    self.plot_hist[device.name][para] += [device_measurement[para]] 
                    
                    self.plot_servers[device.name][para].update(range(-len(self.plot_hist[device.name][para])+1,1),
                                                                self.plot_hist[device.name][para])
                
            
        else:
            device.plot_measure(device_measurement)
    
    def loop(self, n_loops, rate,save = False, comment = '', plot_config = {}, name = None):
        
        measurements = []
        
        
        for i in range(n_loops):
            
            t0 = time.time()
            
            
            if name == None:
                measurements += [self.measure(comment = comment+' '+str(i), plot_config = plot_config, 
                                              save = save)]
            else:
                measurements += [self.measure(comment = comment+' '+str(i), plot_config = plot_config, 
                                              name = name+'_'+str(i), save = save)]
            
            t1 = time.time() - t0
            
            if 1/t1 > rate:
                time.sleep(1/rate - t1)
                print ('desired rate of ',rate,' Hz was reached')
            else:
                print ('desired rate of ',rate,' Hz could NOT be reached')
                
            print ('maximal rate: ',1/t1 )
            
            
            
        
        
    
    def scan(self, scan_parameter, n_loop = 1, rate = 1, save = False, plot_config = {}):
        #scan_parameter = [[device, parameter, from, to, n_steps], ...]
        
        scan_parameter = np.array(scan_parameter, dtype = object)
        
        ndim = len(scan_parameter)
        
        devices_to_change = scan_parameter[:,0]
        parameters_to_change = scan_parameter[:,1]
        change_from = scan_parameter[:,2]
        change_to = scan_parameter[:,3]
        n_steps = scan_parameter[:,4]
        
        linspaces = [np.linspace(change_from[i], change_to[i], n_steps[i]) for i in range(ndim)]
        
        grid = np.meshgrid(*linspaces, indexing = 'ij')
        
        grid = [par.flatten() for par in grid]
        
        info = ''.join([devices_to_change[i]+'-'+parameters_to_change[i]+'_'+str(change_from[i])+'-'+str(change_to[i])+'_'+str(n_steps[i])+'+' for i in range(ndim)])
        
        comment = 'scan of '+info
        
        config_ini = self.device_pool.config
        
        folder_name = 'scan_'+str(datetime.datetime.now()).replace(':','-').replace(' ','_') +'_'+ info
        
        if save:
            os.mkdir(self.io.path+ folder_name)
            
            self.io.save(config_ini, name = folder_name+'/config_at_start',comment = 'before '+comment)
            
        for parameter in np.transpose(grid):
            
            for device, par_name, scan_value in zip(devices_to_change, parameters_to_change, parameter): 
                config_ini[device][par_name] = scan_value
                self.device_pool[device].config = config_ini[device]
            
            
            self.loop(n_loop, rate, comment = comment+', current values: '+str(parameter),
                      name = folder_name+'/'+str(parameters_to_change).replace("'","")+'='+str(parameter),
                      save = save, plot_config = plot_config)  
        

class DeviceMeasurer(Thread):
    def __init__(self, connec, device,  *args, **kwargs):
        self.connec = connec
        self.device = device
        Thread.__init__(self, *args, **kwargs)

    def run(self):
        self.measure()
        
    def measure(self):
        t = time.time()
        device_measurement = self.device.measure()
        print (self.device.name,time.time() - t)
        
        self.connec.send(device_measurement)
        
        
    