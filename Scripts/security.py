import numpy as np
from epics import PV, caput, caget
import ipywidgets as widgets
from IPython.display import display

def shut_down_laser(bt = None):
    caput('F10HU-LMOT715:MOT.VAL', -22.9 - 90)
    
def shut_down_python(bt = None):
    from subprocess import call
    call(["pkill", "python"])
    
def emergency_stop(bt = None):
    shut_down_laser(bt = None)
    shut_down_python(bt = None)



class gui:
    
    def __init__(self):
        

        self.bt_laser = widgets.Button(
            description='STOP LASER',
            disabled=False,
            button_style='warning',layout=widgets.Layout(width='50%', height='100px')
        )

        self.bt_laser.on_click(shut_down_laser)

        self.bt_python = widgets.Button(
            description='STOP PYTHON',
            disabled=False,
            button_style='warning',layout=widgets.Layout(width='50%', height='100px')
        )    

        self.bt_python.on_click(shut_down_python)

        self.bt_stop = widgets.Button(
            description='EMERGENCY STOP',
            disabled=False,
            button_style='danger',layout=widgets.Layout(width='100%', height='200px')
        )

        self.bt_stop.on_click(emergency_stop)


    def show(self):
        display(widgets.VBox([widgets.HBox([self.bt_laser,self.bt_python]),self.bt_stop ]))