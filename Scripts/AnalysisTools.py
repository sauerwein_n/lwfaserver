from uncertainties import unumpy, ufloat
import matplotlib.pyplot as plt
import numpy as np

def make_statistic(xs ,ys, error_x):
    xs = np.array(xs)
    ys = np.array(ys)
    from uncertainties import unumpy
    
    arg = np.argsort(xs)
    
    xs = xs[arg]
    ys = ys[arg]
    
    i = 0
    x_mean = []
    x_error = []
    
    y_mean = []
    y_error = []
    
    n = []
    
    ys_act = []
    xs_act = []
    x_act = 0
    while i < len(xs):
        
        x_act = (x_act * len(xs_act)+ xs[i])/(len(xs_act) + 1)
        ys_act += [ys[i]]
        xs_act += [xs[i]]
        
        if i < len(xs) -1 and abs(xs[i+1] - x_act) < error_x:
            i = i+1
        else:
            
            
            x_mean += [np.mean(xs_act)]
            x_error += [np.std(xs_act, ddof = 1)/np.sqrt(len(xs_act))]
            y_mean += [np.mean(ys_act)]
            y_error += [np.std(ys_act, ddof = 1)/np.sqrt(len(xs_act))]
            
            n += [len(xs_act)]
            
            x_act = 0
            ys_act = []
            xs_act = []
            i = i+1
    return unumpy.uarray(x_mean, x_error), unumpy.uarray(y_mean, y_error), n 

def errorplot(ux, uy, ecolor = 'y', **args):
    
    plt.errorbar(unumpy.nominal_values(ux), unumpy.nominal_values(uy), xerr = unumpy.std_devs(ux),yerr = unumpy.std_devs(uy), fmt= '--o', elinewidth = 10, ecolor = ecolor,alpha = 0.8, **args)
    


    