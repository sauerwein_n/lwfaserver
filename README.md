# LWFAserver README #

This program has been developed to do experiments related to the LWFAcceleration project at PSI Villigen. The purpose of the program is to control the experimental setup. The structure is thought to be so universal so new devices can be add easily and the software can be used for other experiments.

For any questions concerning the software please contact Nick Sauerwein (nicksauerwein@hotmail.de)

## Structure
The progamm consists of a main folder `lwfaserver` with two subfolders (`Data` and `Devices`). The main folder contains the main structure scirpts like (`DataManager.py`, `GUI.py` and `PlottingServer.py`). It also contains the most important IPython Notebook with acutally implements the Interaction with the user (`NotebookGUI.ipynb`). In the following bullet point the porpouse of each of these scripts is explained.
	

- ### Folder `Devices`
	The `Devices` folder contains all the scripts related to devices of the experiments.
	For example the `Interferometer.py` which handles the communication with the camera and data analysis of the interferometer of the gas jet.
	Every device (class) must have a constructor and the functions `get_config` and `set_config(config)`,
	where `config` is a python dictionary which contains the parameters that can be set in the device.
	If the device allows data taking (like picture from the interferometer) the `measure` function handles this. This function returns
	a dictionary of the measurement data. For the live visualisation of the data one should implement a `plot_measure` function.
	It takes the dictionary returned by the `measure` function and usually start up a `bokeh` session to show the results in the browser.
	See section ‘To add new devices’ to read what the structure of such a device has to be in order to be used by the program.
	
- ### Folder `Data`	
	This folder contains all the data from measurements and configurations. The stucture of the content of the folder is user dependend. I simply made folders from each day `EXP170811` with subfolders concerning the experiment.
	
- ### `DataManager.py`
	This script deals with the saving, loading and displaying/plotting of configurations or measurements. The class IO is important to save/load.

- ### `GUI.py`

	In this script the graphical user interphase of the IPython notebook is defined. The main function starts the GUI.
	
- ### `PlottingServer.py`
  In this script the bokeh plotting servers are defined. They handle the displaying of data in the browser. The user can also use inline matplotlib in the notebook, but for faster interactivity I recommend the bokeh servers. There is a server for 1D and a server for 2D data. Take a look at the template device (`Devices\template.py`: function `plot_measure`) to understand the structure.

## To Install
1.	install anaconda python 3.5 from https://www.continuum.io/downloads
2.	install divers for devices, i.e.:

	##### install pypylon:

	```python
	yum install gcc  gcc-c++
	git clone https://github.com/mabl/PyPylon.git
	python setup.py develop
	```
	
	#####install pyepics (http://nazarovsky.ru/2016/07/11/installing-epics-core-system-on-red-hat-linux/):
	```python
	pip install pyepics
	```
	
	##### install delay generator:
	```python
	pip install instrument-kit
	```
	
    ##### install elog:
    ```
    git clone https://github.com/paulscherrerinstitute/py_elog
    cd py_elog/
    python setup.py install
    ```    
        

## Start Programm

### Connection (start of jupyter)
####From local:

- open the terminal in the `lwfaserver` folder and type `jupiter notebook`

####From remote (on lwfaserver):

- add (\ is just to indicate that the linebreak is not real)
 
	  ```
	  alias Xlwfaserver="ssh -Y -tt -L localhost:8890:localhost:8890 -L \
	  localhost:5006:localhost:5006 sauerwein_n@fin-gw.psi.ch \ 
	  'ssh -X -tt -L localhost:8890:localhost:8890 -L \
	  localhost:5006:localhost:5006 sauerwein_n@lwfaserver.psi.ch'"
	  ```	  
   to .bash_profile and source it (change you username)
- run `Xlwfaserver` in the terminal and enter your PSI password
- the jupyter server should run permanently on the lwfaserver so you can easily enter it by opening `localhost:8890` in the browser and enter the password

### Start of GUI

- open `NotebookGUI.ipynb`
- run the cell for the devices needed (initialise devices)
- add devices to the list devices, for example `devices = [delay, fara]` to connect the delay generator and the faraday cup.
- make `IO` object, i.e. `io = IO(path_where_to_save_data)`
- run `GUI.main(devices, io)`

## To add new device

In order to add a new device we recomment either takeing an existing device as template and change  it or there is also a template device in the `Device` folder. The following list explains what sould be changed. In case the device allows measurement the `measure` function has to be specified. 

### Things to set
 - specify the parameters in the beginning of the file
 - write `set_config` , `get_config` functions
 - write `measure` function (not necessary)
 - write `plot_measure` function (not necessary)